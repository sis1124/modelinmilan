import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/user_data.dart';
import 'package:modelinmilan/payment/payment_success.dart';
import 'package:modelinmilan/payment/paypal_payment.dart';
import 'package:modelinmilan/payment/stripe_service.dart';
import 'package:modelinmilan/providers/home_provider.dart';
import 'package:modelinmilan/ui/join_member.dart';
import 'package:provider/provider.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../ui/home.dart';

class MakePayment extends StatefulWidget {
  int price;
  String package;
  MakePayment({this.price,this.package});
  RegisterData userData = RegisterData();


  @override
  _MakePaymentState createState() => _MakePaymentState();
}

class _MakePaymentState extends State<MakePayment> {
  int _value;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  
  


  
  payViaNewCard(BuildContext context, dynamic userid) async {
    ProgressDialog dialog = new ProgressDialog(context);
    dialog.style(
      message: 'Please wait...'
    );
    await dialog.show();
    var response = await StripeService.payWithNewCard(
      amount: widget.price.toString(),
      currency: 'USD'
    );
    await dialog.hide();
    if(response.success == true) {
      ChangeScreen(context, PaymentSuccess(cardId: response.cardId.toString(), cardBrand: response.cardBrand.toString(), userid: userid, price: widget.price, package: widget.package,));
    }
    else{
      Fluttertoast.showToast(
        msg: "Payment failed!",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
        fontSize: 16.0,
      );
      ChangeScreen(context, MakePayment());
    }
  }

  @override
  void initState() {
    super.initState();
    StripeService.init();
  }
  // _paypalSubmit() {
  //   return ChangeScreen(context, PaypalPayment(onFinish: (number) async {
  //     print('order id: '+number);
  //   },
  //   amount: widget.price,
  //   name: widget.userData.name,
  //   phone: widget.userData.phone,
  //   address: widget.userData.address,
  //   town: widget.userData.town,
  //   district: widget.userData.district,
  //   pincode: widget.userData.phone,
  //   packageName: widget.package,
  //   ));
  // }
  // _stripeSubmit() async {
  //   var respone = await StripeService.payWithNewCard(amount: widget.price.toString(), currency: 'USD');
  //   if(respone.success == true) {
  //     Scaffold.of(context).showSnackBar(
  //       SnackBar(
  //         content: Text(respone.message),
  //         duration: Duration(seconds: 3),
  //       )
  //     );
  //   }
  //   else {
  //     Scaffold.of(context).showSnackBar(
  //       SnackBar(
  //         content: Text(respone.message),
  //         duration: Duration(seconds: 3),
  //       )
  //     );
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    var userid = Provider.of<HomeProvider>(context, listen: false).getUserId();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Payment"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              "Confirm order and pay",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            SizedBox(height: 5,),
            Text(
              "Please Make The Payment, after that you can enjoy all the features and benefits.",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 14,
              ),
            ),
            SizedBox(height: 40,),
            Text(
              "Payment Methods",
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
                fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                // Container(
                //   decoration: BoxDecoration(
                //     color: Colors.white,
                //     boxShadow: [
                //       BoxShadow(
                //         color: Colors.grey.withOpacity(0.5),
                //       )
                //     ]
                //   ),
                //   child: MaterialButton(
                //     onPressed: _paypalSubmit,
                //     child: Image.asset("assets/images/paypal.png", width: 80),
                //   ),
                // ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                      )
                    ]
                  ),
                  child: InkWell(
                    onTap: () {
                      payViaNewCard(context, userid);
                    },
                    child: Image.asset("assets/images/stripe.png", width: 100),
                  ),
                ),
              ],
            ),
            SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
}