import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/card_info_model.dart';
import 'package:modelinmilan/providers/home_provider.dart';
import 'package:modelinmilan/ui/main_activity.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:stripe_payment/stripe_payment.dart';

import '../model/user_data.dart';
import '../model/user_data.dart';
import '../ui/home.dart';
import '../utility/api.dart';

class PaymentSuccess extends StatefulWidget {
  var cardId, cardBrand, userid, price, package;
  PaymentSuccess({this.cardId, this.cardBrand, this.userid, this.price, this.package});
  @override
  _PaymentSuccessState createState() => _PaymentSuccessState();
}

class _PaymentSuccessState extends State<PaymentSuccess> {

  postCardData() async {
    var body = jsonEncode(
      {
        "userid": widget.userid,
        "package": widget.package,
        "price": widget.price,
        "id": widget.cardId,
        "brand": widget.cardBrand,
      }
    );
    await http.post(
      Api.paymentSuccessUrl,
      body: body,
    ).then((res) {
      print(res.body.toString());
    }).catchError((e) {
      throw (e);
    });
  }


  @override
  void initState() {
    postCardData();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(50),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Payment Confirmed",
              style: TextStyle(
                fontSize: 18,
                color: Colors.black,
              ),
            ),
            SizedBox(height: 20,),
            Icon(
              Icons.check_circle_outline,
              size: 150,
              color: Colors.cyanAccent,
            ),
            SizedBox(height: 20,),
            Text(
              "You can now enjoy your show",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 16,
              ),
            ),
            SizedBox(height: 20,),
            MaterialButton(
              minWidth: MediaQuery.of(context).size.width,
              color: Colors.cyan,
              child: Text(
                "Go to Home",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  PageTransition(
                    type: PageTransitionType.rightToLeft,
                    child: MainActivity(),
                  ),
                );
                Provider.of<HomeProvider>(context, listen: false).getFeeds();
              },
            ),
          ],
        ),
      ),
    );
  }
}