// import 'dart:core';
// import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:webview_flutter/webview_flutter.dart';
// import 'package:modelinmilan/payment/paypal_service.dart';

// class PaypalPayment extends StatefulWidget {
//   final Function onFinish;
//   var amount, name, phone, address, town, district, pincode;
//   String packageName, packagePrice;

//   PaypalPayment({this.onFinish, this.amount, this.name, this.phone, this.address, this.town, this.district, this.pincode, this.packageName, this.packagePrice});

//   @override
//   State<StatefulWidget> createState() {
//     return PaypalPaymentState();
//   }
// }

// class PaypalPaymentState extends State<PaypalPayment> {
//   GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   String checkoutUrl;
//   String executeUrl;
//   String accessToken;
//   PaypalServices services = PaypalServices();

//   // you can change default currency according to your need
//   Map<dynamic,dynamic> defaultCurrency = {"symbol": "USD ", "decimalDigits": 2, "symbolBeforeTheNumber": true, "currency": "USD"};

//   bool isEnableShipping = false;
//   bool isEnableAddress = false;

//   String returnURL = 'return.example.com';
//   String cancelURL= 'cancel.example.com';


//   @override
//   void initState() {
//     super.initState();

//     Future.delayed(Duration.zero, () async {
//       try {
//         accessToken = await services.getAccessToken();

//         final transactions = getOrderParams();
//         final res =
//             await services.createPaypalPayment(transactions, accessToken);
//         if (res != null) {
//           setState(() {
//             checkoutUrl = res["approvalUrl"];
//             executeUrl = res["executeUrl"];
//           });
//         }
//       } catch (e) {
//         print('exception: '+e.toString());
//         final snackBar = SnackBar(
//           content: Text(e.toString()),
//           duration: Duration(seconds: 10),
//           action: SnackBarAction(
//             label: 'Close',
//             onPressed: () {
//               // Some code to undo the change.
//             },
//           ),
//         );
//         _scaffoldKey.currentState.showSnackBar(snackBar);
//       }
//     });
//   }

  

//   Map<String, dynamic> getOrderParams() {
//       // item name, price and quantity
//     String itemName = widget.packageName;
//     String itemPrice = widget.packagePrice;
//     List items = [
//       {
//         "name": itemName,
//         "price": itemPrice,
//         "currency": defaultCurrency["currency"]
//       }
//     ];


//     // checkout invoice details
//     String totalAmount = widget.amount; //amount, name, phone, address, town, district, pincode;
//     String name = widget.name;
//     String address = widget.address;
//     String town = widget.town;
//     String district = widget.district;
//     String pincode = widget.pincode;
//     String phone = widget.phone;

//     Map<String, dynamic> temp = {
//       "intent": "sale",
//       "payer": {"payment_method": "paypal"},
//       "transactions": [
//         {
//           "amount": {
//             "total": totalAmount,
//             "currency": defaultCurrency["currency"],
//           },
//           "description": "The payment transaction description.",
//           "payment_options": {
//             "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
//           },
//           "item_list": {
//             "items": items,
//             if (isEnableShipping &&
//                 isEnableAddress)
//               "shipping_address": {
//                 "recipient_name": name,
//                 "line1": town,
//                 "line2": "",
//                 "city": address,
//                 "country_code": pincode,
//                 "postal_code": district,
//                 "phone": phone,
//               },
//           }
//         }
//       ],
//       "note_to_payer": "Contact us for any questions on your order.",
//       "redirect_urls": {
//         "return_url": returnURL,
//         "cancel_url": cancelURL
//       }
//     };
//     return temp;
//   }

//   @override
//   Widget build(BuildContext context) {
//     print(checkoutUrl);

//     if (checkoutUrl != null) {
//       return Scaffold(
//         appBar: AppBar(
//           backgroundColor: Theme.of(context).backgroundColor,
//           leading: GestureDetector(
//             child: Icon(Icons.arrow_back_ios),
//             onTap: () => Navigator.pop(context),
//           ),
//         ),
//         body: WebView(
//           initialUrl: checkoutUrl,
//           javascriptMode: JavascriptMode.unrestricted,
//           navigationDelegate: (NavigationRequest request) {
//             if (request.url.contains(returnURL)) {
//               final uri = Uri.parse(request.url);
//               final payerID = uri.queryParameters['PayerID'];
//               if (payerID != null) {
//                 services
//                     .executePayment(executeUrl, payerID, accessToken)
//                     .then((id) {
//                   widget.onFinish(id);
//                   Navigator.of(context).pop();
//                 });
//               } else {
//                 Navigator.of(context).pop();
//               }
//               Navigator.of(context).pop();
//             }
//             if (request.url.contains(cancelURL)) {
//               Navigator.of(context).pop();
//             }
//             return NavigationDecision.navigate;
//           },
//         ),
//       );
//     } else {
//       return Scaffold(
//         key: _scaffoldKey,
//         body: Center(child: Container(child: CircularProgressIndicator())),
//       );
//     }
//   }
// }