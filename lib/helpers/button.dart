import 'package:flutter/material.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';

class Button extends StatelessWidget {
  String text;
  Widget widget_;
  Button({this.text, this.widget_});
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: () {
        ChangeScreen(context, widget_);
      },
      child: Text("$text", style: TextStyle(fontSize: 14),),
      textColor: Colors.grey,
      padding: EdgeInsets.all(15),
    );
  }
}