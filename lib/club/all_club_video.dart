import 'package:flutter/material.dart';
import 'package:modelinmilan/club/view_club_video.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/club_video_model.dart';
import 'package:modelinmilan/utility/api.dart';

class AllClubVideo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Professional Club"),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: FutureBuilder(
              future: Api.getClubVideo(Api.clubVideoUrl),
              builder: (context, snapshot) {
                if(snapshot.hasData) {
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, index) {
                      ClubVideoModel clubVideo = snapshot.data[index];
                      return Container(
                        padding: EdgeInsets.all(5),
                        child: GestureDetector(
                          onTap: () {
                            ChangeScreen(context, ViewClubVideo(url: Api.baseURL + clubVideo.url,));
                          },
                          child: Card(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Stack(
                                  children: [
                                    Container(
                                      child: Image.network(Api.baseURL + clubVideo.feature, height: 150, width: MediaQuery.of(context).size.width, fit: BoxFit.cover,),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 40, left: 90),
                                      child: Icon(Icons.play_arrow, color: Colors.white, size: 100,),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 10,),
                                Text(
                                  clubVideo.title,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: 10,),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                return Center(child: CircularProgressIndicator(),);
              },
            ),
          ),
        ],
      ),
    );
  }
}