import 'package:flutter/material.dart';

class ViewClubImg extends StatelessWidget {
  var img;
  ViewClubImg({this.img});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Image.network(img),
      ),
    );
  }
}