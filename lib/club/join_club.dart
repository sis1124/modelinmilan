import 'package:flutter/material.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/payment/make_payment.dart';
import 'package:modelinmilan/ui/join_member.dart';
import 'package:modelinmilan/ui/membership.dart';

class JoinClub extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color(0xFFFA7D82),
            Color(0xFFFFB295),
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0, bottom: 20.0),
        child: Column(
          children: [
            Text(
              "Professional Club",
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(height: 20,),
            Container(
              padding: EdgeInsets.all(20),
              height: MediaQuery.of(context).size.height / 2,
              width: MediaQuery.of(context).size.width * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                children: [
                  Text(
                    "MONTHLY",
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.grey,
                    )
                  ),
                  SizedBox(height: 5,),
                  Text(
                    "\$5.00/Month",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    )
                  ),
                  SizedBox(height: 20,),
                  Row(
                    children: [
                      Icon(Icons.check, color: Colors.black, size: 16,),
                      SizedBox(width: 10,),
                      Text(
                        "name nulla quam,",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Text(
                    "name nulla quam,,gravida non commodo a soddjfsklkdfjkfj Manris placementr leo.",
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 14,
                    ),
                  ),
                  SizedBox(height: 20,),
                  Row(
                    children: [
                      Icon(Icons.check, color: Colors.black, size: 16,),
                      SizedBox(width: 10,),
                      Text(
                        "name nulla quam,",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Text(
                    "name nulla quam,,gravida non commodo a soddjfsklkdfjkfj Manris placementr leo.",
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 14,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: MaterialButton(
                      onPressed: () {
                        ChangeScreen(context, MakePayment(price: 500, package: "club",));
                      },
                      child: Text("Get Premium"),
                      textColor: Colors.white,
                      padding: EdgeInsets.all(15),
                      color: Colors.cyan,
                      minWidth: MediaQuery.of(context).size.width,
                    ),
                  ),
                ],
              )
            ),
          ],
        )
      ),
    );
  }
}