import 'package:flutter/material.dart';
import 'package:modelinmilan/club/view_club_img.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/club_img_model.dart';
import 'package:modelinmilan/utility/api.dart';

class AllClubImg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Professional Club"),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.only(top: 30, left: 10, right: 10, bottom: 50),
            child: FutureBuilder(
              future: Api.getClubImg(Api.clubImgUrl),
              builder: (context, snapshot) {
                if(snapshot.hasData) {
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, index) {
                      ClubImgModel clubImg = snapshot.data[index];
                      return Container(
                        padding: EdgeInsets.all(20),
                        child: GestureDetector(
                          onTap: () {
                            ChangeScreen(context, ViewClubImg(img: Api.baseURL + clubImg.url));
                          },
                          child: Card(
                            child: Column(
                              children: [
                                Container(
                                  height: 180,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Image.network(Api.baseURL + clubImg.url, height: 180, width: 250, fit: BoxFit.cover,),
                                ),
                                SizedBox(height: 10,),
                                Text(
                                  clubImg.title,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                return Center(child: CircularProgressIndicator(),);
              },
            ),
          ),
        ],
      ),
    );
  }
}