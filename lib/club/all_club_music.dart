import 'package:flutter/material.dart';
import 'package:modelinmilan/club/view_club_music.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/club_music_model.dart';
import 'package:modelinmilan/utility/api.dart';

class AllClubMusic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Professional Club"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: ListView(
          children: [
            Container(
              child: FutureBuilder(
                future: Api.getClubMusic(Api.clubMusicUrl),
                builder: (context, snapshot) {
                  if(snapshot.hasData) {
                    return GridView.builder(
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                      shrinkWrap: true,
                      physics: new NeverScrollableScrollPhysics(),
                      itemCount: snapshot.data.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 1.0,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        ClubMusicModel clubMusic = snapshot.data[index];
                        return InkWell(
                          onTap: () {
                            ChangeScreen(context, ViewClubMusic(url: Api.baseURL + clubMusic.url, title: clubMusic.title, feature: Api.baseURL + clubMusic.feature,));
                          },
                          child: Container(
                            width: 175.0,
                            height: 280.0,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    height: 175.0,
                                    width: 280.0,
                                    child: Stack(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(10.0),
                                            ),
                                            child: Image.network(Api.baseURL + clubMusic.feature, width: 175.0, height: 175.0, fit: BoxFit.cover,),
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 10.0,
                                          right: 10.0,
                                          child: SizedBox(
                                            width: 50.0,
                                            height: 50.0,
                                            child: Icon(
                                              Icons.play_circle_outline,
                                              color: Color(0xfff05042),
                                              size: 40.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5.0, bottom: 5.0),
                                  child: Text(
                                    clubMusic.title,
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  }
                  return Center(child: CircularProgressIndicator(),);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}