import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:modelinmilan/club/all_club_download.dart';
import 'package:modelinmilan/club/all_club_news.dart';
import 'package:modelinmilan/club/club_booklist.dart';
import 'package:modelinmilan/club/club_button.dart';
import 'package:modelinmilan/club/view_club_music.dart';
import 'package:modelinmilan/club/view_club_news.dart';
import 'package:modelinmilan/club/view_club_video.dart';
import 'package:modelinmilan/helpers/button.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/club_download_model.dart';
import 'package:modelinmilan/model/club_music_model.dart';
import 'package:modelinmilan/model/club_news_model.dart';
import 'package:modelinmilan/model/club_video_model.dart';
import 'package:modelinmilan/model/user_email_model.dart';
import 'package:modelinmilan/payment/make_payment.dart';
import 'package:modelinmilan/providers/club_provider.dart';
import 'package:modelinmilan/providers/home_provider.dart';
import 'package:modelinmilan/ui_user/login_with_email.dart';
import 'package:modelinmilan/utility/api.dart';
import 'package:modelinmilan/club/club_worksheet.dart';
import 'package:modelinmilan/widgets/worksheet.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'all_club_music.dart';
import 'all_club_video.dart';
import 'join_club.dart';

class Club extends StatefulWidget {
  @override
  _ClubState createState() => _ClubState();
}

class _ClubState extends State<Club> {
  UserEmail useremail = UserEmail();
  ClubProvider _clubProvider = ClubProvider();

  @override
  void initState() {
    Provider.of<ClubProvider>(context, listen: false).getFeed();
    super.initState();
  }

  getUserEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    useremail.email = prefs.getString('email');
    print(useremail.email);
    return useremail.email;
  }

  @override
  Widget build(BuildContext context) {
    var auto_bloc = Provider.of<HomeProvider>(context);

    return Consumer<ClubProvider>(
      builder: (BuildContext context, ClubProvider clubProvider, Widget child) {
        var _isClubMem = clubProvider.getClubMember();
        return Scaffold(
          appBar: AppBar(
            title: Text("Professional Club"),
          ),
          body: clubProvider.loading == true
          ? Center(child: CircularProgressIndicator(),)
          : RefreshIndicator(
            onRefresh: () => clubProvider.getFeed(),
            child: _isClubMem == false
              ? JoinClub()
              : Container(
                width: MediaQuery.of(context).size.width,
                child: ListView(
                  children: [
                    SizedBox(height: 10,),
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color(0xFF60DFCD),
                            Color(0xFF1E9AFE),
                          ],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Watch Milas's Videos",
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          ClubButton(text: "See More", widget_: AllClubVideo(),),
                        ],
                      ),
                    ),
                    Container(
                      height: 250,
                      child: FutureBuilder(
                        future: Api.getClubVideo(Api.clubVideoUrl),
                        builder: (context, snapshot) {
                          if(snapshot.hasData) {
                            return ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: snapshot.data.length < 4
                              ? snapshot.data.length : 4,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, index) {
                                ClubVideoModel clubVideo = snapshot.data[index];
                                return Container(
                                  child: GestureDetector(
                                    onTap: () {
                                      ChangeScreen(context, ViewClubVideo(url: Api.baseURL + clubVideo.url,));
                                    },
                                    child: Card(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Stack(
                                            children: [
                                              Container(
                                                child: Image.network(Api.baseURL + clubVideo.feature, height: 200, width: MediaQuery.of(context).size.width * 0.7, fit: BoxFit.cover,),
                                              ),
                                              Container(
                                                padding: EdgeInsets.only(top: 50, left: 60),
                                                child: Icon(Icons.play_arrow, color: Colors.white, size: 100,),
                                              ),
                                            ],
                                          ),
                                          SizedBox(height: 10,),
                                          Text(
                                            clubVideo.title,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                          }
                          return Center(child: CircularProgressIndicator(),);
                        },
                      ),
                    ),
                    SizedBox(height: 15,),
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color(0xFF60DFCD),
                            Color(0xFF1E9AFE),
                          ],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Listen Milan's Music",
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          ClubButton(text: "See More", widget_: AllClubMusic(),),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      child: FutureBuilder(
                        future: Api.getClubMusic(Api.clubMusicUrl),
                        builder: (context, snapshot) {
                          if(snapshot.hasData) {
                            return GridView.builder(
                              padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                              shrinkWrap: true,
                              physics: new NeverScrollableScrollPhysics(),
                              itemCount: snapshot.data.length < 4
                                ? snapshot.data.length : 4,
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio: 1.0,
                              ),
                              itemBuilder: (BuildContext context, int index) {
                                ClubMusicModel clubMusic = snapshot.data[index];
                                return InkWell(
                                  onTap: () {
                                    ChangeScreen(context, ViewClubMusic(url: Api.baseURL + clubMusic.url, title: clubMusic.title, feature: Api.baseURL + clubMusic.feature,));
                                  },
                                  child: Container(
                                    width: 175.0,
                                    height: 280.0,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 2,
                                          child: Container(
                                            height: 175.0,
                                            width: 280.0,
                                            child: Stack(
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets.all(5.0),
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius.all(
                                                      Radius.circular(10.0),
                                                    ),
                                                    child: Image.network(Api.baseURL + clubMusic.feature, width: 175.0, height: 175.0, fit: BoxFit.cover,),
                                                  ),
                                                ),
                                                Positioned(
                                                  bottom: 10.0,
                                                  right: 10.0,
                                                  child: SizedBox(
                                                    width: 50.0,
                                                    height: 50.0,
                                                    child: Icon(
                                                      Icons.play_circle_outline,
                                                      color: Color(0xfff05042),
                                                      size: 40.0,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 5.0, bottom: 5.0),
                                          child: Text(
                                            clubMusic.title,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.white,
                                            ),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          }
                          return Center(child: CircularProgressIndicator(),);
                        },
                      ),
                    ),
                    SizedBox(height: 15,),
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color(0xFF60DFCD),
                            Color(0xFF1E9AFE),
                          ],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Downlaod Worksheet",
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          ClubButton(text: "See More", widget_: AllClubDownlaod(),),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      child: FutureBuilder(
                        future: Api.getClubDownlaod(Api.clubDownlaodUrl),
                        builder: (context, snapshot) {
                          if(snapshot.hasData) {
                            return GridView.builder(
                              padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                              shrinkWrap: true,
                              physics: new NeverScrollableScrollPhysics(),
                              itemCount: snapshot.data.length < 4
                              ? snapshot.data.length : 4,
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio: 1.0,
                              ),
                              itemBuilder: (BuildContext context, int index) {
                                ClubDownloadModel clubDownload = snapshot.data[index];
                                return Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  child: ClubWorksheet(
                                    url: Api.baseURL + clubDownload.url,
                                    image: Api.baseURL + clubDownload.image,
                                    title: clubDownload.title,
                                  ),
                                );
                              },
                            );
                          }
                          return Center(child: CircularProgressIndicator(),);
                        },
                      ),
                    ),
                    SizedBox(height: 15,),
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color(0xFF60DFCD),
                            Color(0xFF1E9AFE),
                          ],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Watch Milan's News",
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.white
                            ),
                          ),
                          ClubButton(text: "See More", widget_: AllClubNews()),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width,
                      child: FutureBuilder(
                        future: Api.getClubNews(Api.clubNewsUrl),
                        builder: (context, snapshot) {
                          if(snapshot.hasData) {
                            return ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: snapshot.data.length < 5
                              ? snapshot.data.length : 5,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, index) {
                                ClubNewsModel clubNews = snapshot.data[index];
                                return Container(
                                  child: ClubBookListItem(
                                    image: Api.baseURL + clubNews.image, 
                                    title: clubNews.title, 
                                    description: clubNews.description, 
                                    postedOn: clubNews.postedOn.toString(),
                                  ),
                                );
                              },
                            );
                          }
                          return Center(child: CircularProgressIndicator(),);
                        },
                      ),
                    ),
                    SizedBox(height: 20,),
                  ],
                ),
              ),
          ),
        );
      }
    );
  }
}