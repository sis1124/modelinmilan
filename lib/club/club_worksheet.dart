import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';

class ClubWorksheet extends StatefulWidget {
  var url, image, title;
  ClubWorksheet({this.url, this.image, this.title});
  @override
  _ClubWorksheetState createState() => _ClubWorksheetState();
}

class _ClubWorksheetState extends State<ClubWorksheet> {
  var dio;
  @override
  void initState() {
    super.initState();
    getPermission();
  }

  void getPermission() async {
    await Permission.storage.request().isGranted;
  }

  Future<void> download2(Dio dio, String url, String savePath) async {
    ProgressDialog dialog = new ProgressDialog(context);
    dialog.style(
      message: 'downlaoding...\nplease wait...'
    );
    await dialog.show();

    final response = await dio.get(
      url,
      onReceiveProgress: showDownloadProgress,
      //Received data with List<int>
      options: Options(
        responseType: ResponseType.bytes,
        followRedirects: false,
        validateStatus: (status) {
        return status < 500;
      }),
    );

    //write in download folder
    File file = File(savePath);
    var raf = file.openSync(mode: FileMode.write);
    raf.writeFromSync(response.data);
    await raf.close();
    
    await dialog.hide();
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text("Downlaod Completed"),
        duration: new Duration(milliseconds: 2000),
      )
    );
  }

  showDownloadProgress(received, total) {
    if (total != -1) {
      print((received / total * 100).toStringAsFixed(0) + "%");
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {

      },
      child: Container(
        width: 175.0,
        height: 280.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                height: 175.0,
                width: 280.0,
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                        child: Image.network(widget.image, width: MediaQuery.of(context).size.width,),
                      ),
                    ),
                    Positioned(
                      bottom: 10.0,
                      right: 10.0,
                      child: SizedBox(
                        width: 50.0,
                        height: 50.0,
                        child: RawMaterialButton(
                          onPressed: () async {
                              String path = await ExtStorage.getExternalStoragePublicDirectory(ExtStorage.DIRECTORY_DOWNLOADS);
                              String fullPath = "$path/${widget.title}.pdf";
                              Dio dio = Dio();
                              download2(dio, widget.url, fullPath);
                          },
                          // child: isPlaying
                          //     ? Icon(
                          //   Icons.pause_circle_outline,
                          //   color: Color(0xfff05042),
                          //   size: 40.0,
                          // )
                          child: Icon(
                            Icons.file_download,
                            color: Color(0xfff05042),
                            size: 40.0,
                          ),
                          shape: new CircleBorder(
                              side: BorderSide(
                                  width: 1.0,
                                  color: Color(0xfff05042).withOpacity(0.8))),
                          elevation: 0.0,
                          fillColor: Colors.white.withOpacity(0.8),
                          padding: const EdgeInsets.all(0.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0, bottom: 5.0),
              child: Text(
                widget.title,
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600, color: Colors.white),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}