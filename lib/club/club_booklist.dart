import 'package:flutter/material.dart';
import 'package:modelinmilan/club/view_club_news.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';

class ClubBookListItem extends StatelessWidget {
  var image, title, description, postedOn;
  ClubBookListItem({this.image, this.title, this.description, this.postedOn});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        ChangeScreen(context, ViewClubNews(image: image, title: title, description: description, postedOn: postedOn,));
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Theme.of(context).primaryColor.withOpacity(0.9),
          boxShadow: [
            BoxShadow(
                color: Theme.of(context).focusColor.withOpacity(0.1),
                blurRadius: 5,
                offset: Offset(0, 2)),
          ],
        ),
        child: Row(
          children: <Widget>[
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              elevation: 4,
              child: ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
                child: Image.network(image, width: 70, height: 70, fit: BoxFit.cover),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Hero(
                    tag: title,
                    child: Material(
                      type: MaterialType.transparency,
                      child: Text(
                        title,
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 15,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(
                    description,
                    style: TextStyle(
                      fontSize: 12,
                      color: Theme.of(context).textTheme.headline6.color,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Row(
                    children: [
                      Icon(Icons.calendar_today, color: Theme.of(context).accentColor, size: 12,),
                      SizedBox(width: 3,),
                      Text(
                        postedOn,
                        style: TextStyle(
                          fontSize: 12,
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

