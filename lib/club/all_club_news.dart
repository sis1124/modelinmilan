import 'package:flutter/material.dart';
import 'package:modelinmilan/club/club_booklist.dart';
import 'package:modelinmilan/club/view_club_news.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/club_news_model.dart';
import 'package:modelinmilan/utility/api.dart';

class AllClubNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Professional Club"),
      ),
      body: ListView(
        children: [
          SizedBox(height: 30,),
          Container(
            width: MediaQuery.of(context).size.width,
            child: FutureBuilder(
              future: Api.getClubNews(Api.clubNewsUrl),
              builder: (context, snapshot) {
                if(snapshot.hasData) {
                  return ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, index) {
                      ClubNewsModel clubNews = snapshot.data[index];
                      return Container(
                        child: ClubBookListItem(
                          image: Api.baseURL + clubNews.image, 
                          title: clubNews.title, 
                          description: clubNews.description, 
                          postedOn: clubNews.postedOn.toString(),
                        ),
                      );
                    },
                  );
                }
                return Center(child: CircularProgressIndicator(),);
              },
            ),
          ),
        ],
      )
    );
  }
}