import 'package:flutter/material.dart';
import 'package:modelinmilan/model/club_download_model.dart';
import 'package:modelinmilan/utility/api.dart';
import 'package:modelinmilan/club/club_worksheet.dart';
import 'package:modelinmilan/widgets/worksheet.dart';

class AllClubDownlaod extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Professional Club"),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 30, left: 10, right: 10, bottom: 50),
        child: FutureBuilder(
          future: Api.getClubDownlaod(Api.clubDownlaodUrl),
          builder: (context, snapshot) {
            if(snapshot.hasData) {
              return GridView.builder(
                padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                shrinkWrap: true,
                physics: new NeverScrollableScrollPhysics(),
                itemCount: snapshot.data.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1.0,
                ),
                itemBuilder: (BuildContext context, int index) {
                  ClubDownloadModel clubDownload = snapshot.data[index];
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    child: ClubWorksheet(
                      url: Api.baseURL + clubDownload.url,
                      image: Api.baseURL + clubDownload.image,
                      title: clubDownload.title,
                    ),
                  );
                },
              );
            }
            return Center(child: CircularProgressIndicator(),);
          },
        ),
      ),
    );
  }
}