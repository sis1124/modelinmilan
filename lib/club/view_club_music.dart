import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:assets_audio_player/assets_audio_player.dart';


class ViewClubMusic extends StatefulWidget {
  var url, title, feature;
  ViewClubMusic({this.url, this.title, this.feature});
  @override
  _ViewClubMusicState createState() => _ViewClubMusicState();
}

class _ViewClubMusicState extends State<ViewClubMusic> {
  bool _isPlaying = false;

  final assetsAudioPlayer = AssetsAudioPlayer();

  Future _playAudio() async {
    try {
      if(_isPlaying == false) {
        await assetsAudioPlayer.open(
          Audio.network(widget.url),
          showNotification: true,
        );
      }
      else{
        assetsAudioPlayer.pause();
      }
    } catch (e) {
        print("pause");
        print(e);
    }
    setState(() {
      _isPlaying = !_isPlaying;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: InkWell(
        onTap: () {
          // getAudio();
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 50,),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: CachedNetworkImage(
                      imageUrl: "${widget.feature}",
                      placeholder: (context, url) =>
                          Center(child: CircularProgressIndicator()),
                      errorWidget: (context, url, error) => Image.asset(
                        "assets/images/place.png",
                        fit: BoxFit.cover,
                        height: 175.0,
                        width: 175.0,
                      ),
                      fit: BoxFit.cover,
                      height: 175.0,
                      width: 175.0,
                    ),
                  ),
                ),
                SizedBox(height: 20,),
                Text(
                  "${widget.title}",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Slider(
                  onChanged: (v) {
                    setState(() {
                      assetsAudioPlayer.seek(Duration(seconds: v.toInt() % 100));
                    });
                  },
                  min: 0,
                  value: assetsAudioPlayer.currentPosition.value.inSeconds.toDouble() % 100,
                  max: 100,
                  activeColor: Colors.cyan,
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    IconButton(
                      onPressed: () {
                        assetsAudioPlayer.seekBy(Duration(seconds: -10));
                      },
                      icon: Icon(Icons.skip_previous),
                    ),
                    IconButton(
                      iconSize: 30,
                      onPressed: _playAudio,
                      icon: Icon(_isPlaying ? Icons.stop : Icons.play_arrow),
                    ),
                    IconButton(
                      onPressed: () {
                        assetsAudioPlayer.seekBy(Duration(seconds: 10));
                      },
                      icon: Icon(Icons.skip_next),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}