import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';
import 'package:modelinmilan/widgets/chewie_list_item.dart';

class ViewClubVideo extends StatefulWidget {
  var url;
  ViewClubVideo({this.url});
  @override
  _ViewClubVideoState createState() => _ViewClubVideoState();
}

class _ViewClubVideoState extends State<ViewClubVideo> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;


  void _playVideo() {
    setState(() {
      if(_controller.value.isPlaying) {
        _controller.pause();
      }
      else{
        _controller.play();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: ChewieListItem(
          videoPlayerController: VideoPlayerController.network(widget.url),
          looping: true,
        ),
      ),
    );
  }
}