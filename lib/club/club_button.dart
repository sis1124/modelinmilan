import 'package:flutter/material.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';

class ClubButton extends StatelessWidget {
  String text;
  Widget widget_;
  ClubButton({this.text, this.widget_});
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: () {
        ChangeScreen(context, widget_);
      },
      child: Text("$text", style: TextStyle(fontSize: 14,),),
      textColor: Colors.grey,
      color: Colors.white,
      padding: EdgeInsets.all(10),
    );
  }
}