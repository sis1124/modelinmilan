import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modelinmilan/model/pod_model.dart';

const String API_KEY = "AIzaSyBlrZkzcyJw0L7VSihyd88o9rtlwfWbfIM";

///music player variable
int curPos = 0;
List<PodModel> curPlayList = List();

//color
///primary color of your app
Color primary = Color(0xffFD0262);

///secondary color of your app
Color secondary = Color(0xffFDBF92);

///current player state
enum PlayerState { stopped, playing, paused }

///music player state
AudioPlayerState audioPlayerState;

///music player instance
AudioPlayer audioPlayer;

///player state
PlayerState playerState = PlayerState.stopped;

///get is currently playing
dynamic get isPlaying => playerState == PlayerState.playing;

///get is currently paushed
dynamic get isPaused => playerState == PlayerState.paused;

///song total duration
Duration duration;

/// song current position
Duration position;

///is song play from local
bool isLocal = false;

///media player mode
PlayerMode mode = PlayerMode.MEDIA_PLAYER;

///remote url
String url;

///duration listner
StreamSubscription durationSubscription;

///position change listner
StreamSubscription positionSubscription;

///complete listner
StreamSubscription playerCompleteSubscription;

///player error listner
StreamSubscription playerErrorSubscription;

///player state listner
StreamSubscription playerStateSubscription;
