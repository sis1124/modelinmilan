import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class SharedPref {
  read(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key));
  }

  save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }


  saveString(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  Future<bool> setLogin(bool status) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setBool("log_in", status);
  }
  Future<bool> getLogIn() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool("log_in") ?? false;
  }
}