import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:modelinmilan/model/club_download_model.dart';
import 'package:modelinmilan/model/club_music_model.dart';
import 'package:modelinmilan/model/club_news_model.dart';
import 'package:modelinmilan/model/club_video_model.dart';
import 'package:modelinmilan/model/comments_data.dart';
import 'package:modelinmilan/model/download_model.dart';
import 'package:modelinmilan/model/pod_model.dart';
import 'package:modelinmilan/model/podcast_model.dart';
import 'package:modelinmilan/model/slider_model.dart';

import '../podo/category.dart';
import 'keys.dart';

class Api {
  static String baseURL = "http://appbuff.co.uk/milanapp/";
  static String popular = baseURL + "api.php?get=top";
  static String breaking = baseURL + "api.php?get=breaking";
  static String trends = baseURL + "api.php?get=trends";
  static String searchUrl = baseURL + "api.php?get=search&char=";
  static String imageURL = baseURL + "images/";
  static String podcastURL = baseURL + "api.php?get=podcast";
  static String downloadURL = baseURL + "api.php?get=dw";
  static String commentsUrl = baseURL + "api.php?get=comments";

  static String member = baseURL + "member_preview.php";
  
  static String clubMem = baseURL + "club_preview.php";
  static String clubImgUrl = baseURL + "api.php?get=club_img";
  static String clubVideoUrl = baseURL + "api.php?get=club_video";
  static String clubMusicUrl = baseURL + "api.php?get=club_music";
  static String clubDownlaodUrl = baseURL + "api.php?get=club_download";
  static String clubNewsUrl = baseURL + "api.php?get=club_news";

  static String sliderUrl = baseURL + "api.php?get=slider";
  static String subscriberUrl = baseURL + "miss_smart_subscriber.php";

  static String paymentSuccessUrl = baseURL + "insert_payment.php";


  static Future<CategoryFeed> getNews(String url) async {
    var res = await http.get(url);
    CategoryFeed category;
    if (res.statusCode == 200) {
      print(res.body);
      final jsonResponse = json.decode(res.body);
      category = CategoryFeed.fromJson(jsonResponse);
    } else {
      throw ("Error ${res.statusCode}");
    }
    return category;
  }

  static Future<PodCastData> getPodCast(String url) async {
    List<PodModel> _catRadioList = [];
    var res = await http.get(url);
    PodCastData pod_cast;
    if (res.statusCode == 200) {
      print(res.body);
      final jsonResponse = json.decode(res.body);
      pod_cast = PodCastData.fromJson(jsonResponse);
//print(jsonResponse['feed']['pod_cast']);
      var data1 = jsonResponse['feed']['pod_cast'] as List;
//print(data1);
      _catRadioList = data1
          .map((data) => PodModel.fromJson(data as Map<String, dynamic>))
          .toList();
      curPlayList = _catRadioList;
    } else {
      throw ("Error ${res.statusCode}");
    }
    return pod_cast;
  }

  static Future<DownloadData> getDownload(String url) async {
    var res = await http.get(url);
    DownloadData dw;
    if (res.statusCode == 200) {
      print(res.body);
      final jsonResponse = json.decode(res.body);
      dw = DownloadData.fromJson(jsonResponse);
    } else {
      throw ("Error ${res.statusCode}");
    }
    return dw;
  }

  static Future<CommentsData> getComments(String url) async {
    var res = await http.get(url);
    //print(res.data);
    CommentsData category;
    if (res.statusCode == 200) {
      final jsonResponse = json.decode(res.body);
      category = CommentsData.fromJson(jsonResponse);
    } else {
      throw ("Error ${res.statusCode}");
    }
    return category;
  }

  static Future<bool> updateCount(String url) async {
    var res = await http.get(url);
    if (res.statusCode == 200) {
    } else {
      throw ("Error ${res.statusCode}");
    }
    return true;
  }

  

  static Future<List<ClubVideoModel>> getClubVideo(String url) async {
    var res = await http.get(url);
    return clubVideoModelFromJson(res.body);
  }

  static Future<List<ClubMusicModel>> getClubMusic(String url) async {
    var res = await http.get(url);
    return clubMusicModelFromJson(res.body);
  }

  static Future<List<ClubDownloadModel>> getClubDownlaod(String url) async {
    var res = await http.get(url);
    return clubDownloadModelFromJson(res.body);
  }

  static Future<List<ClubNewsModel>> getClubNews(String url) async {
    var res = await http.get(url);
    return clubNewsModelFromJson(res.body);
  }

  static Future<List<SliderModel>> getSlider(String url) async {
    var res = await http.get(url);
    return sliderModelFromJson(res.body);
  }

}

