class LoginRequestData {
  String email = '';
  String password = '';
}

class RegisterData {
  String srNo;
  String name = '';
  String fname = '';
  String dob = '';
  String phone = '';
  String email = '';
  String password = '';
  String cpassword = '';
  String address = '';
  String town = '';
  //String tahsil = '';
  String district = '';
  String state = '';
  String pincode = '';
  String image;

  RegisterData({
    this.srNo,
    this.name,
    this.fname,
    this.dob,
    this.phone,
    this.email,
    this.password,
    this.cpassword,
    this.address,
    this.town,
    //this.tahsil,
    this.district,
    this.state,
    this.pincode,
    this.image,
  });

  RegisterData.fromJson(Map<String, dynamic> json) {
        srNo= json['user']['SrNo'];
        name= json['user']['fullname'];
        fname= json['user']['fathername'];
        dob= json['user']['dbo'];
        phone= json['user']['phone'];
        email= json['user']['email'];
        password= "";
        cpassword= "";
        address= json['user']['address'];
        town= json['user']['town'];
        //tahsil= json['user']['tahsil'];
        district= json['user']['district'];
        state= json['user']['state'];
        pincode= json['user']['pin'];
        image= json['user']['profile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SrNo']= this.srNo;
    data['name']= this.name;
    data['fname']= this.fname;
    data['dob']= this.dob;
    data['phone']= this.phone;
    data['email']= this.email;
    data['password']= this.password;
    data['cpassword']= this.cpassword;
    data['address']= this.address;
    data['town']= this.town;
    //data['tahsil']= this.tahsil;
    data['district']= this.district;
    data['state']= this.state;
    data['pincode']= this.pincode;
    data['image']= this.image;
    return data;
  }
}