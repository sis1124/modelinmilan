import 'dart:convert';

List<WebseriesModel> webseriesModelFromJson(String str) => List<WebseriesModel>.from(json.decode(str).map((x) => WebseriesModel.fromJson(x)));

String webseriesModelToJson(List<WebseriesModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class WebseriesModel {
    WebseriesModel({
        this.id,
        this.url,
        this.youtubeUrl,
    });

    String id;
    String url;
    String youtubeUrl;

    factory WebseriesModel.fromJson(Map<String, dynamic> json) => WebseriesModel(
        id: json["id"],
        url: json["url"],
        youtubeUrl: json["youtube_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "youtube_url": youtubeUrl,
    };
}