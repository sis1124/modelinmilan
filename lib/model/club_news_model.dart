import 'dart:convert';

List<ClubNewsModel> clubNewsModelFromJson(String str) => List<ClubNewsModel>.from(json.decode(str).map((x) => ClubNewsModel.fromJson(x)));

String clubNewsModelToJson(List<ClubNewsModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ClubNewsModel {
    ClubNewsModel({
        this.id,
        this.image,
        this.title,
        this.description,
        this.postedOn,
    });

    String id;
    String image;
    String title;
    String description;
    DateTime postedOn;

    factory ClubNewsModel.fromJson(Map<String, dynamic> json) => ClubNewsModel(
        id: json["id"],
        image: json["image"],
        title: json["title"],
        description: json["description"],
        postedOn: DateTime.parse(json["posted_on"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "image": image,
        "title": title,
        "description": description,
        "posted_on": "${postedOn.year.toString().padLeft(4, '0')}-${postedOn.month.toString().padLeft(2, '0')}-${postedOn.day.toString().padLeft(2, '0')}",
    };
}
