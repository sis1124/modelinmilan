import 'dart:convert';

List<ClubMusicModel> clubMusicModelFromJson(String str) => List<ClubMusicModel>.from(json.decode(str).map((x) => ClubMusicModel.fromJson(x)));

String clubMusicModelToJson(List<ClubMusicModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ClubMusicModel {
    ClubMusicModel({
        this.id,
        this.url,
        this.title,
        this.feature,
    });

    String id;
    String url;
    String title;
    String feature;

    factory ClubMusicModel.fromJson(Map<String, dynamic> json) => ClubMusicModel(
        id: json["id"],
        url: json["url"],
        title: json["title"],
        feature: json["feature"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "title": title,
        "feature": feature,
    };
}