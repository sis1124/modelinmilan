import 'package:flutter/material.dart';

class DownloadData {
  String version;
  String encoding;
  Feed feed;

  DownloadData({this.version, this.encoding, this.feed});

  DownloadData.fromJson(Map<String, dynamic> json) {
    version = json['version'];
    encoding = json['encoding'];
    feed = json['feed'] != null ? new Feed.fromJson(json['feed']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['version'] = this.version;
    data['encoding'] = this.encoding;
    if (this.feed != null) {
      data['feed'] = this.feed.toJson();
    }
    return data;
  }
}

class Feed with ChangeNotifier {
  List<dwLink> linkdw = [];
  bool loading = true;

  Feed({this.linkdw});

  Feed.addList() {
    if(linkdw == null) {
      loading = true;
      notifyListeners();
    }
  }

  Feed.fromJson(Map<String, dynamic> json) {
    if (json['download'] != null) {
      linkdw = new List<dwLink>();
      json['download'].forEach((v) {
        linkdw.add(new dwLink.fromJson(v));
        loading = false;
        notifyListeners();
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.linkdw != null) {
      data['link'] = this.linkdw.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class dwLink {
  String id = '';
  String title = '';
  String dw_img = '';
  String dw_url = '';
  String dw_file = '';
  String dw_count = '';
  String published = '';


  dwLink({
    this.id,
    this.title,
    this.dw_img,
    this.dw_url,
    this.dw_file,
    this.dw_count,
    this.published,
  });

  dwLink.fromJson(Map<String, dynamic> json) {
    id= json['id'];
    title= json['title'];
    dw_img= json['dw_img'];
    dw_url= json['dw_url'];
    dw_file= json['dw_file'];
    dw_count= json['dw_count'];
    published= json['published'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id']= this.id;
    data['title']= this.title;
    data['dw_img']= this.dw_img;
    data['dw_url']= this.dw_url;
    data['dw_file']= this.dw_file;
    data['dw_count']= this.dw_count;
    data['published']= this.published;
    return data;
  }
}