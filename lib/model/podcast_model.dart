class PodCastData {
  String version;
  String encoding;
  Feed feed;

  PodCastData({this.version, this.encoding, this.feed});

  PodCastData.fromJson(Map<String, dynamic> json) {
    version = json['version'];
    encoding = json['encoding'];
    feed = json['feed'] != null ? new Feed.fromJson(json['feed']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['version'] = this.version;
    data['encoding'] = this.encoding;
    if (this.feed != null) {
      data['feed'] = this.feed.toJson();
    }
    return data;
  }
}

class Feed {
  List<Linkk> link;

  Feed({this.link});

  Feed.fromJson(Map<String, dynamic> json) {
    if (json['pod_cast'] != null) {
      link = new List<Linkk>();
      json['pod_cast'].forEach((v) {
        link.add(new Linkk.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.link != null) {
      data['link'] = this.link.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Linkk {
  String id = '';
  String title = '';
  String path_img = '';
  String path_mp3 = '';
  String publish = '';


  Linkk({
    this.id,
    this.title,
    this.path_img,
    this.path_mp3,
    this.publish,
  });

  Linkk.fromJson(Map<String, dynamic> json) {
    id= json['id'];
    title= json['title'];
    path_img= json['pod_img'];
    path_mp3= json['pod_mp3'];
    publish= json['published'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id']= this.id;
    data['title']= this.title;
    data['pod_img']= this.path_img;
    data['pod_mp3']= this.path_mp3;
    data['published']= this.publish;
    return data;
  }
}
