import 'dart:convert';

List<ClubVideoModel> clubVideoModelFromJson(String str) => List<ClubVideoModel>.from(json.decode(str).map((x) => ClubVideoModel.fromJson(x)));

String clubVideoModelToJson(List<ClubVideoModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ClubVideoModel {
    ClubVideoModel({
        this.id,
        this.url,
        this.title,
        this.feature,
    });

    String id;
    String url;
    String title;
    String feature;

    factory ClubVideoModel.fromJson(Map<String, dynamic> json) => ClubVideoModel(
        id: json["id"],
        url: json["url"],
        title: json["title"],
        feature: json["feature"] == null ? null : json["feature"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "title": title,
        "feature": feature == null ? null : feature,
    };
}