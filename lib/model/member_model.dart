import 'dart:convert';

MemberModel memberModelFromJson(String str) => MemberModel.fromJson(json.decode(str));

String memberModelToJson(MemberModel data) => json.encode(data.toJson());

class MemberModel {
    MemberModel({
        this.status,
        this.statusText,
    });

    String status;
    String statusText;

    factory MemberModel.fromJson(Map<String, dynamic> json) => MemberModel(
        status: json["status"],
        statusText: json["status_text"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "status_text": statusText,
    };
}