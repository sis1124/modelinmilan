import 'dart:convert';

List<ClubDownloadModel> clubDownloadModelFromJson(String str) => List<ClubDownloadModel>.from(json.decode(str).map((x) => ClubDownloadModel.fromJson(x)));

String clubDownloadModelToJson(List<ClubDownloadModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ClubDownloadModel {
    ClubDownloadModel({
        this.id,
        this.url,
        this.image,
        this.title,
    });

    String id;
    String url;
    String image;
    String title;

    factory ClubDownloadModel.fromJson(Map<String, dynamic> json) => ClubDownloadModel(
        id: json["id"],
        url: json["url"],
        image: json["image"],
        title: json["title"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "image": image,
        "title": title,
    };
}