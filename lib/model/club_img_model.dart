import 'dart:convert';

List<ClubImgModel> clubImgModelFromJson(String str) => List<ClubImgModel>.from(json.decode(str).map((x) => ClubImgModel.fromJson(x)));

String clubImgModelToJson(List<ClubImgModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ClubImgModel {
    ClubImgModel({
        this.id,
        this.url,
        this.title,
    });

    String id;
    String url;
    String title;

    factory ClubImgModel.fromJson(Map<String, dynamic> json) => ClubImgModel(
        id: json["id"],
        url: json["url"],
        title: json["title"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "title": title,
    };
}