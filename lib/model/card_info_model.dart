import 'dart:convert';

CardInfoModel cardInfoModelFromJson(String str) => CardInfoModel.fromJson(json.decode(str));

String cardInfoModelToJson(CardInfoModel data) => json.encode(data.toJson());

class CardInfoModel {
    CardInfoModel({
        this.created,
        this.id,
        this.livemode,
        this.type,
        this.billingDetails,
        this.card,
    });

    int created;
    String id;
    bool livemode;
    String type;
    BillingDetails billingDetails;
    Card card;

    factory CardInfoModel.fromJson(Map<String, dynamic> json) => CardInfoModel(
        created: json["created"],
        id: json["id"],
        livemode: json["livemode"],
        type: json["type"],
        billingDetails: BillingDetails.fromJson(json["billingDetails"]),
        card: Card.fromJson(json["card"]),
    );

    Map<String, dynamic> toJson() => {
        "created": created,
        "id": id,
        "livemode": livemode,
        "type": type,
        "billingDetails": billingDetails.toJson(),
        "card": card.toJson(),
    };
}

class BillingDetails {
    BillingDetails({
        this.address,
    });

    Address address;

    factory BillingDetails.fromJson(Map<String, dynamic> json) => BillingDetails(
        address: Address.fromJson(json["address"]),
    );

    Map<String, dynamic> toJson() => {
        "address": address.toJson(),
    };
}

class Address {
    Address();

    factory Address.fromJson(Map<String, dynamic> json) => Address(
    );

    Map<String, dynamic> toJson() => {
    };
}

class Card {
    Card({
        this.addressLine1,
        this.addressLine2,
        this.brand,
        this.country,
        this.expMonth,
        this.expYear,
        this.funding,
        this.last4,
    });

    dynamic addressLine1;
    dynamic addressLine2;
    String brand;
    String country;
    int expMonth;
    int expYear;
    String funding;
    String last4;

    factory Card.fromJson(Map<String, dynamic> json) => Card(
        addressLine1: json["addressLine1"],
        addressLine2: json["addressLine2"],
        brand: json["brand"],
        country: json["country"],
        expMonth: json["expMonth"],
        expYear: json["expYear"],
        funding: json["funding"],
        last4: json["last4"],
    );

    Map<String, dynamic> toJson() => {
        "addressLine1": addressLine1,
        "addressLine2": addressLine2,
        "brand": brand,
        "country": country,
        "expMonth": expMonth,
        "expYear": expYear,
        "funding": funding,
        "last4": last4,
    };
}