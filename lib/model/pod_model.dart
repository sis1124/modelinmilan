class PodModel {
  String id;
  String title = '';
  String path_img = '';
  String path_mp3 = '';
  String publish = '';

  PodModel({
    this.id,
    this.title,
    this.path_img,
    this.path_mp3,
    this.publish,
  });

  ///get data
  factory PodModel.fromJson(Map<String, dynamic> parsedJson) {
    return PodModel(
      id: parsedJson['id'].toString(),
      title: parsedJson['title'].toString(),
      path_img: parsedJson['pod_img'].toString(),
      path_mp3: parsedJson['pod_mp3'].toString(),
      publish: parsedJson['published'].toString(),
    );
  }
}

