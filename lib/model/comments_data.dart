class CommentsData {
  String version;
  String encoding;
  Feed feed;

  CommentsData({this.version, this.encoding, this.feed});

  CommentsData.fromJson(Map<String, dynamic> json) {
    version = json['version'];
    encoding = json['encoding'];
    feed = json['feed'] != null ? new Feed.fromJson(json['feed']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['version'] = this.version;
    data['encoding'] = this.encoding;
    if (this.feed != null) {
      data['feed'] = this.feed.toJson();
    }
    return data;
  }
}

class Feed {
  List<Link> link;

  Feed({this.link});

  Feed.fromJson(Map<String, dynamic> json) {
    if (json['gallery'] != null) {
      link = new List<Link>();
      json['gallery'].forEach((v) {
        link.add(new Link.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.link != null) {
      data['link'] = this.link.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Link {
  String srno;
  String title = '';
  String path = '';
  String user = '';

  Link({this.srno, this.title, this.path, this.user});

  Link.fromJson(Map<String, dynamic> json) {
    srno = json['srno'];
    title = json['img_title'];
    path = json['img_path'];
    user = json['user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['srno'] = this.srno;
    data['title'] = this.title;
    data['path'] = this.path;
    data['user'] = this.user;
    return data;
  }
}
