import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:modelinmilan/ui_user/login_with_email.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../providers/app_provider.dart';
import '../providers/favorites_provider.dart';
import '../providers/home_provider.dart';
import '../utility/constants.dart';
import '../utility/sharedPref.dart';
import '../utility/api.dart';
import 'favorites.dart';
import '../ui_user/login.dart';
import '../ui_user/ProfilePage.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  SharedPref sharedPref = SharedPref();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  static String isLogin;
  String _name;
  String _strSrNo;
  static String _profile;

  String _setting = "false";

  // Group Value for Radio Button.
  String _Lang;

  _ProfileState() {
    loadSharedPrefs();
    //_checkSupport();
  }

  // _checkSupport() {
  //   http.get(Api.supportUrl).then((res) {
  //     if (res.statusCode == 200) {
  //       // If the server did return a 200 OK response,
  //       // then parse the JSON.
  //       setState(() {
  //         _setting = res.body.toString().trim();
  //       });
  //       print(_setting);
  //       return _setting;
  //     } else {
  //       // If the server did not return a 200 OK response,
  //       // then throw an exception.
  //       throw Exception('Failed to load status');
  //     }
  //   });
  // }

  loadSharedPrefs() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        isLogin = prefs.getString("isLogin") == null
            ? "0"
            : prefs.getString("isLogin");
        _strSrNo = prefs.getString("SrNo") == null
            ? "Please LogIn"
            : prefs.getString("SrNo");
        _name = prefs.getString("name") == null
            ? "Please LogIn"
            : prefs.getString("name");
        _Lang = prefs.getString("isLang") == null
            ? "0"
            : prefs.getString("isLang");
        _profile = prefs.getString("profile") == null
            ? null
            : prefs.getString("profile");
        print(_profile);
      });
    } catch (e) {
      print(e.toString());
    }
  }

  List items = [
    // {
    //   "icon": Icons.supervised_user_circle,
    //   "title": "Available",
    // },
    // {
    //   "icon": Icons.contacts,
    //   "title": "Contact Us",
    //   "page": Contact(),
    // },
    {
      "icon": Icons.favorite,
      "title": "Favorites",
      "page": Favorites(),
    },
    {"icon": Icons.brightness_6, "title": "Dark Mode"},
    {
      "icon": Icons.exit_to_app,
      "title": "Logout",
    },
    // {
    //   "icon": Icons.chat,
    //   "title": "Live Support",
    // },
//    {
//      "icon": Icons.language,
//      "title": "Language",
//    }
  ];

  _sendToLoginPage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginWithEmail()),
    );
  }

  // _sendToChatPage() async {
  //   Navigator.push(
  //     context,
  //     MaterialPageRoute(builder: (context) => Chats()),
  //   );
  //   Provider.of<ChatProvider>(context, listen: false).getFeeds(_strSrNo);
  // }

  _sendToProfilePage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ProfilePage()),
    ).whenComplete(loadSharedPrefs);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Settings",
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: new Stack(fit: StackFit.loose, children: <Widget>[
              new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    width: 140.0,
                    height: 140.0,
                    child: ClipOval(
                      child: CachedNetworkImage(
                        imageUrl:
                            _profile == null ? "" : Api.baseURL + _profile,
                        placeholder: (context, url) =>
                            Center(child: CircularProgressIndicator()),
                        errorWidget: (context, url, error) => Image.asset(
                          "assets/images/user.png",
                          fit: BoxFit.cover,
                          height: 140.0,
                          width: 140.0,
                        ),
                        fit: BoxFit.cover,
                        height: 140.0,
                        width: 140.0,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                  padding: EdgeInsets.only(top: 90.0, right: 100.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        child: new CircleAvatar(
                          backgroundColor: Colors.red,
                          radius: 25.0,
                          child: new Icon(
                            isLogin == "1" ? Icons.edit : Icons.lock_outline,
                            color: Colors.white,
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            isLogin == "1"
                                ? _sendToProfilePage()
                                : scaffoldKey.currentState.showSnackBar(
                                    SnackBar(
                                        content: new Text("You are not LogIn."),
                                        duration: const Duration(
                                            milliseconds: 1500)));
                          });
                        },
                      )
                    ],
                  )),
            ]),
          ),
          Padding(
              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Hi! $_name',
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          //RadioGroup(),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: <Widget>[
          //     Text('Language =>',
          //         style: TextStyle(fontSize: 16.0)),
          //     Radio(
          //       value: '0',
          //       groupValue: _Lang,
          //       onChanged: (val) {
          //         setState(() {
          //           _Lang = '0';
          //           sharedPref.saveString('isLang', '0');
          //           Provider.of<HomeProvider>(context, listen: false).getFeeds();
          //         });
          //       },
          //     ),
          //     Text(
          //       'English',
          //       style: new TextStyle(fontSize: 16.0),
          //     ),
          //
          //     Radio(
          //       value: '1',
          //       groupValue: _Lang,
          //       onChanged: (val) {
          //         setState(() {
          //           _Lang = '1';
          //           sharedPref.saveString('isLang', '1');
          //           Provider.of<HomeProvider>(context, listen: false).getFeeds();
          //         });
          //       },
          //     ),
          //     Text(
          //       'Kannada',
          //       style: new TextStyle(
          //         fontSize: 16.0,
          //       ),
          //     ),
          //   ],
          // ),
          ListView.separated(
            padding: EdgeInsets.symmetric(horizontal: 10),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              if (items[index]['title'] == "Dark Mode") {
                return SwitchListTile(
                  secondary: Icon(
                    items[index]['icon'],
                  ),
                  title: Text(
                    items[index]['title'],
                  ),
                  value: Provider.of<AppProvider>(context).theme ==
                          Constants.lightTheme
                      ? false
                      : true,
                  onChanged: (v) {
                    if (v) {
                      Provider.of<AppProvider>(context, listen: false)
                          .setTheme(Constants.darkTheme, "dark");
                    } else {
                      Provider.of<AppProvider>(context, listen: false)
                          .setTheme(Constants.lightTheme, "light");
                    }
                  },
                );
              }
              if (items[index]['title'] == "Logout" ||
                  items[index]['title'] == "LogIn") {
                items[index]['title'] = isLogin == "0" ? "LogIn" : "Logout";
              }
              return ListTile(
                onTap: () {
                  if (items[index]['title'] == "info") {
                  } else if (items[index]['title'] == "Logout") {
                    sharedPref.remove("user");
                    sharedPref.remove("isLogin");
                    sharedPref.remove("name");
                    sharedPref.remove("profile");
                    sharedPref.remove("email");
                    loadSharedPrefs();
                    Scaffold.of(context).showSnackBar(SnackBar(
                        content: new Text("You have securely logged out!"),
                        duration: const Duration(milliseconds: 500)));
//                setState(() {
//                  items[index]['title'] = "LogIn";
//                });
                  } else if (items[index]['title'] == "Available") {
                    //
                  } else if (items[index]['title'] == "LogIn") {
                    _sendToLoginPage();
                  // } else if (items[index]['title'] == "Live Support") {
                  //   _sendToChatPage();
                  } else {
                    Provider.of<FavoritesProvider>(context, listen: false)
                        .getFeed();
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.rightToLeft,
                        child: items[index]['page'],
                      ),
                    );
                  }
                },
                leading: items[index]['title'] == "Available"
                    ? _setting == "false"
                    ? Icon(Icons.supervised_user_circle, color: Colors.red)
                    : Icon(Icons.supervised_user_circle,
                    color: Colors.green)
                    :Icon(
                  items[index]['icon'],
                ),
//                trailing: items[index]['title'] == "Contact Us"
//                    ? _setting == "false"
//                        ? Icon(Icons.supervised_user_circle, color: Colors.red)
//                        : Icon(Icons.supervised_user_circle,
//                            color: Colors.green)
//                    : null,
                title: items[index]['title'] == "Available"
                    ? _setting == "false"
                    ? Text(items[index]['title'],style: TextStyle(color: Colors.red),)
                      : Text(items[index]['title'],style: TextStyle(color: Colors.green),)
                    :Text(
                  items[index]['title'],
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return Divider();
            },
          ),
        ],
      ),
    );
  }
}

class RadioGroup extends StatefulWidget {
  @override
  RadioGroupWidget createState() => RadioGroupWidget();
}

class RadioGroupWidget extends State {

  // Default Radio Button Selected Item When App Starts.
  String radioButtonItem = 'English';

  // Group Value for Radio Button.
  int id = 0;

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('App Language => ' + '$radioButtonItem',
                style: TextStyle(fontSize: 16.0))
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Radio(
              value: 0,
              groupValue: id,
              onChanged: (val) {
                setState(() {
                  radioButtonItem = 'English';
                  id = 0;
                });
              },
            ),
            Text(
              'English',
              style: new TextStyle(fontSize: 16.0),
            ),

            Radio(
              value: 1,
              groupValue: id,
              onChanged: (val) {
                setState(() {
                  radioButtonItem = 'Kannada';
                  id = 1;
                });
              },
            ),
            Text(
              'Kannada',
              style: new TextStyle(
                fontSize: 16.0,
              ),
            ),
          ],
        ),
      ],
    );
  }
}