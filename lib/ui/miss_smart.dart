import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';

import '../utility/api.dart';

class MissSmart extends StatefulWidget {
  @override
  _MissSmartState createState() => _MissSmartState();
}

class _MissSmartState extends State<MissSmart> with SingleTickerProviderStateMixin {
  TabController _controller;
  final formKey = GlobalKey<FormState>();

  var name, email;

  _launchTutoring() async {
    const url = 'https://www.differentcolorbrowns.com/milanamiya/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  
  Future _subscribeSubmit() async {
    if(formKey.currentState.validate()){
      formKey.currentState.save();
      final url = Api.subscriberUrl;
      final res = await http.post(
        url,
        headers: {'Accept': 'Application/json',},
        body: {
          'name': this.name,
          'email': this.email,
        }
      ).then((res) {
        print(res.body.toString());
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: new Text("Subscription done"),
            duration: const Duration(milliseconds: 500)
          )
        );
      }).catchError((err) {
        print(err);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = TabController(vsync: this, initialIndex: 0, length: 2);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: TabBar(
            onTap: (index) {},
            controller: _controller,
            indicatorColor: Color.fromRGBO(209, 2, 99, 1),
            tabs: [
              Tab(text: "Stay Updated",),
              Tab(text: "Tutoring",)
            ],
          ),
        ),
        Container(
          height: 370,
          child: TabBarView(
            controller: _controller,
            children: [
              Column(
                children: [
                  SizedBox(height: 20,),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Text(
                      "Enter your name and email address to subscribe to Model in Milan app and receive notifications of update by email",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20)),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 20),
                      child: Form(
                        key: formKey,
                        child: Column(
                          children: [
                            SizedBox(height: 10,),
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 25.0,
                                    spreadRadius: 2.0,
                                  ),
                                ]
                              ),
                              child: TextFormField(
                                decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.account_circle, color: Colors.redAccent,),
                                  hintText: "Name",
                                  border: InputBorder.none,
                                ),
                                validator: (value) {
                                  if(value.isEmpty) {
                                    return 'please enter your name';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  this.name = value;
                                },
                              ),
                            ),
                            SizedBox(height: 20,),
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 25.0,
                                    spreadRadius: 2.0,
                                  ),
                                ]
                              ),
                              child: TextFormField(
                                decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.email, color: Colors.redAccent,),
                                  hintText: "Email",
                                  border: InputBorder.none,
                                ),
                                validator: (value) {
                                  if(value.isEmpty) {
                                    return 'please enter email address';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  this.email = value;
                                },
                              ),
                            ),
                            SizedBox(height: 20,),
                            MaterialButton(
                              onPressed: _subscribeSubmit,
                              child: Text("Subscribe"),
                              textColor: Colors.white,
                              padding: EdgeInsets.all(15),
                              color: Color.fromRGBO(209, 2, 99, 1),
                              minWidth: MediaQuery.of(context).size.width,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),

              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Transform your life through education. Learn on your schedule. Anywhere, anytime.Start learning today!",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: 20,),
                    Text(
                      "Transform your life through education. Learn on your schedule. Anywhere, anytime.Start learning today!",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: 20,),
                    Text(
                      "Transform your life through education. Learn on your schedule. Anywhere",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: 20,),
                    Text(
                      "Transform your life through education. Learn on your schedule. Anywhere, anytime.Start learning today!",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    MaterialButton(
                      onPressed: _launchTutoring,
                      child: Text("Sign Up"),
                      textColor: Colors.white,
                      padding: EdgeInsets.all(15),
                      color: Color.fromRGBO(209, 2, 99, 1),
                      minWidth: MediaQuery.of(context).size.width,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}