import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/providers/home_provider.dart';
import 'package:modelinmilan/providers/member_provider.dart';
import 'package:modelinmilan/ui/membership.dart';
import 'package:modelinmilan/ui_user/login_with_email.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/channel_model.dart';
import '../model/video_model.dart';
import '../services/api_service.dart';
import 'video_screen.dart';

class VideoHome extends StatefulWidget {
  @override
  _VideoHomeScreenState createState() => _VideoHomeScreenState();
}

class _VideoHomeScreenState extends State<VideoHome> {
  Channel _channel;
  bool _isLoading = false;
  MemberProvider _memberProvider = MemberProvider();

  @override
  void initState() {
    super.initState();
    _initChannel();
  }

  _initChannel() async {
    Channel channel = await APIService.instance
        .fetchChannel(channelId: 'UCUHBzD_xxPqxlyaAyIe3ROQ'); //Add channel here
    setState(() {
      _channel = channel;
    });
  }

  _buildProfileInfo() {
    return Container(
      padding: EdgeInsets.all(10.0),
      height: 200.0,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      top: 32, left: 8, right: 8, bottom: 16),
                  child: Container(
                    decoration: BoxDecoration(
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Color(0xFFFFB295).withOpacity(0.6),
                            offset: const Offset(1.1, 4.0),
                            blurRadius: 8.0),
                      ],
                      gradient: LinearGradient(
                        colors: [
                          Color(0xFFFA7D82),
                          Color(0xFFFFB295),
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                      borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(8.0),
                        bottomLeft: Radius.circular(8.0),
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 54, left: 16, right: 16, bottom: 8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            _channel.title,
                            textAlign: TextAlign.center,
                            style: GoogleFonts.lato(
                              textStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 26.0,
                                letterSpacing: 0.2,
                                color: Color(0xFFFFFFFF),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 8, bottom: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '${_channel.subscriberCount} subscribers',
                                    style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                        //fontFamily: FintnessAppTheme.fontName,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16,
                                        letterSpacing: 0.2,
                                        color: Color(0xFFFFFFFF),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: -2,
                  child: Container(
                    width: 100,
                    height: 85,
                    decoration: BoxDecoration(
                      color: Color(0xFFFAFAFA).withOpacity(0.2),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 8,
                  child: SizedBox(
                    width: 80,
                    height: 80,
                    child: CircleAvatar(
                      backgroundColor: Theme.of(context).accentColor,
                      backgroundImage: NetworkImage(_channel.profilePictureUrl),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildVideo(Video video) {
    return GestureDetector(
      onTap: () async {
        var _isMem = Provider.of<HomeProvider>(context, listen: false).getMember();
        if(_isMem != true) {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          if(prefs.getString("profile") == null) {
            return ChangeScreen(context, LoginWithEmail());
          }
          else{
            return ChangeScreen(context, Membership());
          }
        }
        else {
          return Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => VideoScreen(id: video.id),
            ),
          );
        }
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
        padding: EdgeInsets.all(10.0),
        height: 140.0,
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Color(0xFFFE95B6).withOpacity(0.6),
                offset: const Offset(1.1, 4.0),
                blurRadius: 8.0),
          ],
          gradient: LinearGradient(
            colors: [
              Color(0xFFFE95B6).withOpacity(0.5),
              Color(0xFFFF5287).withOpacity(0.8),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: const BorderRadius.only(
            bottomRight: Radius.circular(8.0),
            bottomLeft: Radius.circular(8.0),
            topLeft: Radius.circular(8.0),
            topRight: Radius.circular(8.0),
          ),
        ),
        child: Row(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              child: Image(
                width: 150.0,
                image: NetworkImage(video.thumbnailUrl),
              ),
            ),
            SizedBox(width: 10.0),
            Expanded(
              child: Text(
                video.title,
                style: GoogleFonts.lato(
                  textStyle: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 20.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _loadMoreVideos() async {
    _isLoading = true;
    List<Video> moreVideos = await APIService.instance
        .fetchVideosFromPlaylist(playlistId: _channel.uploadPlaylistId);
    List<Video> allVideos = _channel.videos..addAll(moreVideos);
    setState(() {
      _channel.videos = allVideos;
    });
    _isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Milan's World"),
      ),
      body: _channel != null
          ? NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollDetails) {
                if (!_isLoading &&
                    _channel.videos.length != int.parse(_channel.videoCount) &&
                    scrollDetails.metrics.pixels ==
                        scrollDetails.metrics.maxScrollExtent) {
                  //_loadMoreVideos();
                }
                return false;
              },
              child: ListView.builder(
                itemCount: 1 + _channel.videos.length,
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    return _buildProfileInfo();
                  }
                  Video video = _channel.videos[index - 1];
                  return _buildVideo(video);
                },
              ),
            )
          : Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  Theme.of(context).accentColor, // Red
                ),
              ),
            ),
    );
  }
}
