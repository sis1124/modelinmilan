import 'package:flutter/material.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/payment/make_payment.dart';

import 'membership.dart';

class JoinMember extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Updrade Your Membership")
      ),
      body: Column(
        children: [
          SizedBox(height: 50,),
          Text(
            "Choose your Package",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          SizedBox(height: 20,),
          MaterialButton(
            onPressed: () {
              ChangeScreen(context, MakePayment(price: 500, package: "month",));
            },
            child: Container(
              padding: EdgeInsets.only(top: 50.0, left: 10.0, right: 10.0, bottom: 5),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 120,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color(0xFFFA7D82),
                      Color(0xFFFFB295),
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(height: 20,),
                      Text(
                        "\$5.00 Month",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      SizedBox(height: 10,),
                      Text(
                        "Click here to achive.",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                        ),
                      ),
                    ],
                  ),
                )
              ),
            ),
          ),
          SizedBox(height: 20,),
          MaterialButton(
            onPressed: () {
              ChangeScreen(context, MakePayment(price: 5000, package: "annual",));
            },
            child: Container(
              padding: EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0, bottom: 5.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 120,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color(0xFF60DFCD),
                      Color(0xFF1E9AFE),
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(height: 20,),
                      Text(
                        "\$50.00 Annual",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      SizedBox(height: 10,),
                      Text(
                        "Click here to achive. save up to 10\$",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                        ),
                      ),
                    ],
                  ),
                )
              ),
            ),
          )
        ],
      ),
    );
  }
}