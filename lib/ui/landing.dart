import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../utility/constants.dart';
import '../utility/custom_button.dart';
import 'main_activity.dart';

class Main1View extends StatefulWidget {
  @override
  _Main1ViewState createState() => _Main1ViewState();
}

class _Main1ViewState extends State<Main1View> {

  nextPage() async {
    Navigator.pushReplacement(
      context,
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: MainActivity(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //var _theme = Theme.of(context);
    //var width = MediaQuery.of(context).size.width;
    //var widgetWidth = width - AppSizes.sidePadding * 2;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.height,
                //width: width,
                decoration: BoxDecoration(
                  gradient:
                  LinearGradient(
                      colors: [
                        const Color(0xff43cea2),
                        //Colors.cyan,
                        const Color(0xff185a9d)
                      ],
                      begin: const FractionalOffset(0.0, 1.0),
                      end: const FractionalOffset(1.0, 0.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.mirror
                  )
                  // LinearGradient(
                  //     begin: Alignment.topRight,
                  //     end: Alignment.bottomLeft,
                  //     colors: [
                  //       Colors.cyan.withOpacity(0.9),
                  //       Colors.lightBlueAccent.withOpacity(0.9),
                  //     ]),
//                image: DecorationImage(
//                  fit: BoxFit.fill,
//                  image: AssetImage('assets/images/splash-home.png'),
//               ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        left: AppSizes.sidePadding,
                      ),
                      //width: width / 2,
                      child: Text(
                        '\nThe “Model In Milan” app by Different Color Browns was designed to Motivate, Educate and Inspire children to chase their dreams. Milan Amiyah not only aims to be a role model to all children but also would like all children to find the role model within themselves.',
                        style: TextStyle(
                          fontSize: 35.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontFamily: "Joyful_Juliana",
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: AppSizes.sidePadding,
                          bottom: AppSizes.sidePadding,
                          top: AppSizes.sidePadding),
                      width: 160,
                      child: OpenFlutterButton(
                        title: 'Get Started!',
                        width: 160,
                        height: 48,
                        onPressed: () => {
                          //do something
                          nextPage(),
                        },
//                      onPressed: (() =>
//                          widget.changeView(
//                              changeType: ViewChangeType.Forward)
//                      ),
                      ),
                    )
                  ],
                )),
            // SizedBox(
            //   height: 10,
            // ),
            // Container(
            //   padding: EdgeInsets.symmetric(horizontal: 15),
            //   margin: EdgeInsets.symmetric(horizontal: 10),
            //   decoration: BoxDecoration(
            //     gradient: LinearGradient(
            //       stops: [0.015, 0.015],
            //       colors: [
            //         Color.fromRGBO(209, 2, 99, 1),
            //         //Theme.of(context).backgroundColor
            //         Colors.white,
            //       ],
            //     ),
            //   ),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: <Widget>[
            //       Text(
            //         "What New?",
            //         style: TextStyle(
            //           fontSize: 15,
            //           fontWeight: FontWeight.bold,
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
            // SizedBox(
            //   height: 5,
            // ),
            // Container(
            //   height: 250,
            //   child: Center(
            //     child: ListView.builder(
            //       padding: EdgeInsets.symmetric(horizontal: 5),
            //       scrollDirection: Axis.horizontal,
            //       itemCount: homeProvider.top.feed.entry.length== null ? 0 : homeProvider.top.feed.entry.length,
            //       shrinkWrap: true,
            //       itemBuilder: (BuildContext context, int index) {
            //         Entry entry = homeProvider.top.feed.entry[index];
            //         return Padding(
            //           padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            //           child: BookCard(
            //             img: entry.coverImage,
            //             entry: entry,
            //           ),
            //         );
            //       },
            //     ),
            //   ),
            // ),

//          OpenFlutterBlockHeader(
//            width: widgetWidth,
//            title: 'New',
//            linkText: 'View All',
//            onLinkTap: () =>
//            {
//              Navigator.of(context).pushNamed(OpenFlutterEcommerceRoutes.shop,
//                  arguments: CategoriesParameters(0))
//            },
//            description: 'You’ve never seen it before!',
//          ),
//          OpenFlutterProductListView(
//            width: widgetWidth,
//            products: widget.products,
//            onFavoritesTap: ((Product product) =>
//            {
//              BlocProvider.of<HomeBloc>(context).add(
//                  HomeAddToFavoriteEvent(
//                      isFavorite: !product.isFavorite,
//                      product: product
//                  )
//              )
//            }),
//          ),
//          OpenFlutterButton(
//            title: 'Next Home Page',
//            width: 160,
//            height: 48,
//            onPressed: (() =>
//                widget.changeView(changeType: ViewChangeType.Forward)),
//          )
          ],
        ),
      ),
    );
  }
}
