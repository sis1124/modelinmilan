import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../model/podcast_model.dart';
import 'package:assets_audio_player/assets_audio_player.dart';


class MusicPlayer extends StatefulWidget {
  final String img;
  final String title;
  final Linkk entrypod;
  final int index;

  MusicPlayer({
    Key key,
    //List<PodModel> pod,
    @required this.img,
    @required this.title,
    @required this.entrypod,
    @required this.index,
  });

  @override
  _MusicPlayerState createState() => _MusicPlayerState();
}

class _MusicPlayerState extends State<MusicPlayer> {
  bool _isPlaying = false;

  final assetsAudioPlayer = AssetsAudioPlayer();

  Future _playAudio() async {
    try {
      if(_isPlaying == false) {
        await assetsAudioPlayer.open(
          Audio.network(widget.entrypod.path_mp3),
          showNotification: true,
        );
      }
      else{
        assetsAudioPlayer.pause();
      }
    } catch (e) {
        print("pause");
        print(e);
    }
    setState(() {
      _isPlaying = !_isPlaying;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: InkWell(
        onTap: () {
          // getAudio();
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 50,),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: CachedNetworkImage(
                      imageUrl: "${widget.img}",
                      placeholder: (context, url) =>
                          Center(child: CircularProgressIndicator()),
                      errorWidget: (context, url, error) => Image.asset(
                        "assets/images/place.png",
                        fit: BoxFit.cover,
                        height: 175.0,
                        width: 175.0,
                      ),
                      fit: BoxFit.cover,
                      height: 175.0,
                      width: 175.0,
                    ),
                  ),
                ),
                SizedBox(height: 20,),
                Text(
                  "${widget.title}",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Slider(
                  onChanged: (v) {
                    setState(() {
                      assetsAudioPlayer.seek(Duration(seconds: v.toInt() % 100));
                    });
                  },
                  min: 0,
                  value: assetsAudioPlayer.currentPosition.value.inSeconds.toDouble() % 100,
                  max: 100,
                  activeColor: Colors.cyan,
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    IconButton(
                      onPressed: () {
                        assetsAudioPlayer.seekBy(Duration(seconds: -10));
                      },
                      icon: Icon(Icons.skip_previous),
                    ),
                    IconButton(
                      iconSize: 30,
                      onPressed: _playAudio,
                      icon: Icon(_isPlaying ? Icons.stop : Icons.play_arrow),
                    ),
                    IconButton(
                      onPressed: () {
                        assetsAudioPlayer.seekBy(Duration(seconds: 10));
                      },
                      icon: Icon(Icons.skip_next),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}