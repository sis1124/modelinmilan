//import 'package:admob_flutter/admob_flutter.dart';
//import 'package:firebase_analytics/firebase_analytics.dart';
import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modelinmilan/helpers/button.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/download_model.dart';
import 'package:modelinmilan/model/slider_model.dart';
import 'package:modelinmilan/payment/stripe_service.dart';
import 'package:modelinmilan/providers/member_provider.dart';
import 'package:modelinmilan/ui/membership.dart';
import 'package:modelinmilan/ui/miss_smart.dart';
import 'package:modelinmilan/ui/pod_cast.dart';
import 'package:modelinmilan/ui/settings.dart';
import 'package:modelinmilan/ui/video_home.dart';
import 'package:modelinmilan/ui/webseries.dart';
import 'package:modelinmilan/ui_user/ProfilePage.dart';
import 'package:modelinmilan/ui_user/login.dart';
import 'package:modelinmilan/ui_user/login_with_email.dart';
import 'package:modelinmilan/utility/sharedPref.dart';
import 'package:modelinmilan/widgets/chewie_list_item.dart';
import 'package:modelinmilan/widgets/pod_card.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:video_player/video_player.dart';

import '../podo/category.dart';
import '../providers/home_provider.dart';
import '../utility/constants.dart';
import '../utility/api.dart';
//import '../helper/click_count.dart';
import '../ui/genre.dart';
import '../ui/download.dart';
import '../ui/news.dart';



import '../model/channel_model.dart';
import '../model/video_model.dart';
import '../services/api_service.dart';
import 'video_screen.dart';

import 'package:google_fonts/google_fonts.dart';

import '../services/api_service.dart';
import 'video_screen.dart';

import '../widgets/book_list_item.dart';
import '../widgets/home_card.dart';
import '../widgets/worksheet.dart';

import '../model/podcast_model.dart';
import '../model/pod_model.dart';
import 'package:http/http.dart' as http;


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>{
  Channel _channel;



  bool _status = true;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final FocusNode myFocusNode = FocusNode();
  static String isLogin;
  String _strSrNo,
      _strName,
      _strFather,
      _strDate,
      _strMobile,
      _strAddress,
      _strTown,
      _strDistrict,
      _strPin;
  static String _profile;
  SharedPreferences prefs;
  File _image;
  bool saveStart = false;
  bool saveComplete = false;
  Channel channel;
  bool _isLoading = false;
  bool _isProfile = true;

  MemberProvider _memberProvider = MemberProvider();
  



  loadSharedPrefs() async {
    try {
      prefs = await SharedPreferences.getInstance();
      _isProfile = prefs.getString("profile") == null ? true : false;
    } catch (e) {
      print("shared prefs err: ");
      print(e.toString());
    }
  }
  
  
  

  @override
  void initState() {
    initChannel();
    loadSharedPrefs();
    super.initState();
  }


  initChannel() async {
    Channel channel = await APIService.instance
        .fetchChannel(channelId: 'UCUHBzD_xxPqxlyaAyIe3ROQ'); //Add channel here
    setState(() {
      _channel = channel;
    });
  }

  _launchURL() async {
    const url = 'https://www.differentcolorbrowns.com/shop';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  //ClickCount clickCount = ClickCount();
  //final FirebaseAnalytics analytics = FirebaseAnalytics();

  // _HomeState() {
  //   _sendAnalyticsEvent();
  // }
  //
  // Future<void> _sendAnalyticsEvent() async {
  //   await analytics.logAppOpen();
  // }
 

  _buildProfileInfo() {
    return Container(
      padding: EdgeInsets.all(10.0),
      height: 200.0,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      top: 32, left: 8, right: 8, bottom: 16),
                  child: Container(
                    decoration: BoxDecoration(
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Color(0xFFFFB295).withOpacity(0.6),
                            offset: const Offset(1.1, 4.0),
                            blurRadius: 8.0),
                      ],
                      gradient: LinearGradient(
                        colors: [
                          Color(0xFFFA7D82),
                          Color(0xFFFFB295),
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                      borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(8.0),
                        bottomLeft: Radius.circular(8.0),
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 54, left: 16, right: 16, bottom: 8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            _channel.title,
                            textAlign: TextAlign.center,
                            style: GoogleFonts.lato(
                              textStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 26.0,
                                letterSpacing: 0.2,
                                color: Color(0xFFFFFFFF),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 8, bottom: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '${_channel.subscriberCount} subscribers',
                                    style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                        //fontFamily: FintnessAppTheme.fontName,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16,
                                        letterSpacing: 0.2,
                                        color: Color(0xFFFFFFFF),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: -2,
                  child: Container(
                    width: 100,
                    height: 85,
                    decoration: BoxDecoration(
                      color: Color(0xFFFAFAFA).withOpacity(0.2),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 8,
                  child: SizedBox(
                    width: 80,
                    height: 80,
                    child: CircleAvatar(
                      backgroundColor: Theme.of(context).accentColor,
                      backgroundImage: NetworkImage(_channel.profilePictureUrl),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildVideo(Video video) {
    return GestureDetector(
      onTap: () async {
        var _isMem = Provider.of<HomeProvider>(context, listen: false).getMember();
        if(_isMem != true) {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          if(prefs.getString("profile") == null) {
            return ChangeScreen(context, LoginWithEmail());
          }
          else{
            return ChangeScreen(context, Membership());
          }
        }
        else {
          return Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => VideoScreen(id: video.id),
            ),
          );
        }
        
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
        padding: EdgeInsets.all(10.0),
        height: 140.0,
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Color(0xFFFE95B6).withOpacity(0.6),
                offset: const Offset(1.1, 4.0),
                blurRadius: 8.0),
          ],
          gradient: LinearGradient(
            colors: [
              Color(0xFFFE95B6).withOpacity(0.5),
              Color(0xFFFF5287).withOpacity(0.8),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: const BorderRadius.only(
            bottomRight: Radius.circular(8.0),
            bottomLeft: Radius.circular(8.0),
            topLeft: Radius.circular(8.0),
            topRight: Radius.circular(8.0),
          ),
        ),
        child: Row(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              child: Image(
                width: 150.0,
                image: NetworkImage(video.thumbnailUrl),
              ),
            ),
            SizedBox(width: 10.0),
            Expanded(
              child: Text(
                video.title,
                style: GoogleFonts.lato(
                  textStyle: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 20.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeProvider>(
      builder: (BuildContext context, HomeProvider homeProvider, Widget child) {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              "${Constants.appName}",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            actions: [
              Container(
                width: 80.0,
                height: 50.0,
                child: ClipOval(
                  child: _isProfile
                      ? FlatButton(
                        child: Text(
                          "Log in",
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.grey,
                          ),
                        ),
                        onPressed: () {
                          ChangeScreen(context, LoginWithEmail(),);
                        },
                      )
                      : InkWell(
                        onTap: () {
                          ChangeScreen(context, Profile());
                        },
                        child: CachedNetworkImage(
                            imageUrl: _profile == null
                                ? ""
                                : Api.baseURL + _profile,
                            placeholder: (context, url) => Center(
                                child: CircularProgressIndicator()),
                            errorWidget: (context, url, error) =>
                                Image.asset(
                              "assets/images/user.png",
                              height: 35.0,
                              width: 35.0,
                            ),
                            height: 35.0,
                            width: 35.0,
                          ),
                      ),
                ),
              ),
            ],
          ),
          body: homeProvider.loading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : RefreshIndicator(
                  onRefresh: () => homeProvider.getFeeds(),
                  child: ListView(
                    children: <Widget>[
                      getSearchBarUI(context),
                      SizedBox(
                        height: 2,
                      ),
                      // Center(
                      //   child: AdmobBanner(
                      //     adUnitId: Constants.strBannerAdId,
                      //     adSize: AdmobBannerSize.BANNER,
                      //   ),
                      // ),

                      Container(
                        height: 220,
                        child: FutureBuilder(
                          future: Api.getSlider(Api.sliderUrl),
                          builder: (context, snapshot) {
                            if(snapshot.hasData) {
                              return ListView.builder(
                                itemCount: snapshot.data.length,
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (BuildContext context, index) {
                                  SliderModel slider = snapshot.data[index];
                                  return MaterialButton(
                                    onPressed: () async {
                                      var url = slider.url;
                                      if (await canLaunch(url)) {
                                        await launch(url);
                                      } else {
                                        throw 'Could not launch $url';
                                      }
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.all(5),
                                      child: Stack(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10),
                                            ),
                                            height: 200,
                                            width: MediaQuery.of(context).size.width,
                                            child: Image.network(Api.baseURL + slider.image, height: 200, fit: BoxFit.cover,),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(top: 150, left: 20,),
                                            child: Text(
                                              slider.title,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              );
                            }
                            return Center(child: CircularProgressIndicator(),);
                          },
                        ),
                      ),





                      // Container(
                      //   height: 250,
                      //   child: Center(
                      //     child: ListView.builder(
                      //       padding: EdgeInsets.symmetric(horizontal: 15),
                      //       scrollDirection: Axis.horizontal,
                      //       itemCount: homeProvider.top.feed.entry.length,
                      //       shrinkWrap: true,
                      //       itemBuilder: (BuildContext context, int index) {
                      //         Entry entry = homeProvider.top.feed.entry[index];
                      //         return Padding(
                      //           padding: EdgeInsets.symmetric(
                      //               horizontal: 5, vertical: 10),
                      //           child: BookCard(
                      //             img: entry.coverImage,
                      //             entry: entry,
                      //           ),
                      //         );
                      //       },
                      //     ),
                      //   ),
                      // ),
                      SizedBox(
                        height: 10,
                      ),
                      // Container(
                      //   padding: EdgeInsets.symmetric(horizontal: 15),
                      //   margin: EdgeInsets.symmetric(horizontal: 20),
                      //   decoration: BoxDecoration(
                      //     gradient: LinearGradient(
                      //       stops: [0.015, 0.015],
                      //       colors: [
                      //         Color.fromRGBO(209, 2, 99, 1),
                      //         Colors.white,
                      //         //Theme.of(context).backgroundColor
                      //       ],
                      //     ),
                      //   ),
                      //   child: Row(
                      //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //     children: <Widget>[
                      //       Text(
                      //         "Categories",
                      //         style: TextStyle(
                      //           fontSize: 15,
                      //           fontWeight: FontWeight.bold,
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      // Container(
                      //   height: 60,
                      //   child: Center(
                      //     child: ListView.builder(
                      //       padding: EdgeInsets.symmetric(horizontal: 15),
                      //       scrollDirection: Axis.horizontal,
                      //       itemCount: homeProvider.top.feed.link.length,
                      //       shrinkWrap: true,
                      //       itemBuilder: (BuildContext context, int index) {
                      //         Link link = homeProvider.top.feed.link[index];

                      //         return Padding(
                      //           padding: EdgeInsets.symmetric(
                      //               horizontal: 5, vertical: 10),
                      //           child: Container(
                      //             decoration: BoxDecoration(
                      //               color: Theme.of(context).accentColor,
                      //               borderRadius: BorderRadius.all(
                      //                 Radius.circular(5),
                      //               ),
                      //             ),
                      //             child: InkWell(
                      //               borderRadius: BorderRadius.all(
                      //                 Radius.circular(5),
                      //               ),
                      //               onTap: () {
                      //                 //clickCount.showInterstitial();
                      //                 Navigator.push(
                      //                   context,
                      //                   PageTransition(
                      //                     type: PageTransitionType.rightToLeft,
                      //                     child: Genre(
                      //                       title: "${link.title}",
                      //                       url: link.href,
                      //                     ),
                      //                   ),
                      //                 );
                      //               },
                      //               child: Center(
                      //                 child: Padding(
                      //                   padding: EdgeInsets.symmetric(
                      //                       horizontal: 10),
                      //                   child: Text(
                      //                     "${link.title}",
                      //                     style: TextStyle(
                      //                       color:
                      //                           Theme.of(context).primaryColor,
                      //                       fontWeight: FontWeight.w500,
                      //                     ),
                      //                   ),
                      //                 ),
                      //               ),
                      //             ),
                      //           ),
                      //         );
                      //       },
                      //     ),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 2,
                      // ),
                      // Center(
                      //   child: AdmobBanner(
                      //     adUnitId: Constants.strBannerAdId,
                      //     adSize: AdmobBannerSize.BANNER,
                      //   ),
                      // ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [
                              Color.fromRGBO(209, 2, 99, 1),
                              Colors.white,
                              //Theme.of(context).backgroundColor
                            ],
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Watch Milas's Web Series",
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            //Button(text: "See More", widget_: Webseries()),
                          ],
                        ),
                      ),
                      Container(
                        height: 500,
                        child: _channel != null
                        ? NotificationListener<ScrollNotification>(
                            onNotification: (ScrollNotification scrollDetails) {
                              if (!_isLoading &&
                                  _channel.videos.length != int.parse(_channel.videoCount) &&
                                  scrollDetails.metrics.pixels ==
                                      scrollDetails.metrics.maxScrollExtent) {
                                //_loadMoreVideos();
                              }
                              return false;
                            },
                            child: ListView.builder(
                              itemCount: 1 + _channel.videos.length,
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                if (index == 0) {
                                  return _buildProfileInfo();
                                }
                                Video video = _channel.videos[index - 1];
                                return _buildVideo(video);
                              },
                            ),
                          )
                        : Center(
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                Theme.of(context).accentColor, // Red
                              ),
                            ),
                          ),
                      ),
                      // Container(
                      //   height: 250,
                      //   child: FutureBuilder(
                      //     future: Api.getWebseries(Api.webseries),
                      //     builder: (context, snapshot) {
                      //       if(snapshot.hasData) {
                      //         return ListView.builder(
                      //           scrollDirection: Axis.horizontal,
                      //           itemCount: snapshot.data.length > 5 ? 5 : snapshot.data.length,
                      //           shrinkWrap: true,
                      //           itemBuilder: (BuildContext context, index) {
                      //             WebseriesModel webseries = snapshot.data[index];
                      //             return ChewieListItem(
                      //               videoPlayerController: VideoPlayerController.network(webseries.url),
                      //               looping: true,
                      //             );
                      //           },
                      //         );
                      //       }
                      //       return Center(child: CircularProgressIndicator(),);
                      //     },
                      //   ),
                      // ),
                      SizedBox(
                        height: 10,
                      ),
                      

                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [
                              Color.fromRGBO(209, 2, 99, 1),
                              Colors.white,
                              //Theme.of(context).backgroundColor
                            ],
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Keep Up/Miss Smart",
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      // miss smart section
                      MissSmart(),



                      

                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text(
                              "Download Worksheet!",
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Button(text: "See More", widget_: Downloads()),
                          ],
                        ),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [
                              Color.fromRGBO(209, 2, 99, 1),
                              Colors.white,
                              //Theme.of(context).backgroundColor
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),

                      homeProvider.dw.feed.loading == true
                      ? Center(child: CircularProgressIndicator())
                      : GridView.builder(
                        padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                        shrinkWrap: true,
                        physics: new NeverScrollableScrollPhysics(),
                        itemCount: homeProvider.dw.feed.linkdw.length < 4
                            ? homeProvider.dw.feed.linkdw.length
                            : 4,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 1.0,
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          dwLink entrydw = homeProvider.dw.feed.linkdw[index];
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            child: WorkSheet(
                              img: entrydw.dw_img,
                              title: entrydw.title,
                              entry: entrydw,
                            ),
                          );
                        },
                      ),
                      // SizedBox(
                      //   height: 2,
                      // ),
                      // Center(
                      //   child: AdmobBanner(
                      //     adUnitId: Constants.strBannerAdId,
                      //     adSize: AdmobBannerSize.MEDIUM_RECTANGLE,
                      //   ),
                      // ),
                      SizedBox(
                        height: 5,
                      ),

                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [
                              Color.fromRGBO(209, 2, 99, 1),
                              Colors.white,
                              //Theme.of(context).backgroundColor
                            ],
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Podcast",
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Button(text: "See More", widget_: PodCast()),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      GridView.builder(
                        padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                        shrinkWrap: true,
                        physics: new NeverScrollableScrollPhysics(),
                        itemCount: homeProvider.podpro.feed.link.length < 4
                        ? homeProvider.podpro.feed.link.length
                        : 4,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 1.0,
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          Linkk entrypod = homeProvider.podpro.feed.link[index];
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            child: PodCard(
                              img: entrypod.path_img,
                              title: entrypod.title,
                              index: index,
                              entrypod: entrypod,
                              //play: _play,
                              //pod:PodModel.fromJson(json),
                            ),
                          );
                        },
                      ),

                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [
                              Color.fromRGBO(209, 2, 99, 1),
                              Colors.white,
                              //Theme.of(context).backgroundColor
                            ],
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Milan's Creations",
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Text(
                              "Enhance your child'a creativity & learning with our Educational Supplies! Shop the best study kits for your children to make their education interesting.",
                              style: TextStyle(
                                fontSize: 16,
                                color: Color.fromRGBO(209, 2, 99, 1),
                              ),
                            ),
                            SizedBox(height: 20,),
                            Text(
                              "Shop Online Today!",
                              style: TextStyle(
                                fontSize: 16,
                                color: Color.fromRGBO(209, 2, 99, 1),
                              ),
                            ),
                            SizedBox(height: 20,),
                            MaterialButton(
                              onPressed: _launchURL,
                              child: Text("Buy Now"),
                              textColor: Colors.white,
                              padding: EdgeInsets.all(15),
                              color: Color.fromRGBO(209, 2, 99, 1),
                              minWidth: MediaQuery.of(context).size.width,
                            ),
                          ],
                        ),
                      ),


                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [
                              Color.fromRGBO(209, 2, 99, 1),
                              Colors.white,
                              //Theme.of(context).backgroundColor
                            ],
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Breaking News",
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Button(text: "See More", widget_: News()),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListView.builder(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: homeProvider.recent.feed.entrycategory.length < 5 
                            ? homeProvider.recent.feed.entrycategory.length : 5,
                        itemBuilder: (BuildContext context, int index) { 
                          Entry entrynews = homeProvider.recent.feed.entrycategory[index];
                          return Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: BookListItem(
                              img: entrynews.coverImage,
                              title: entrynews.title,
                              author: entrynews.category[0],
                              desc: entrynews.summary
                                  .replaceAll(RegExp(r"<[^>]*>"), ''),
                              entrynews: entrynews,
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
        );
      },
    );
  }

  Widget getSearchBarUI(BuildContext context) {
    // Create a text controller and use it to retrieve the current value
    // of the TextField.
    final _txtSearch = TextEditingController();

    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16, top: 8, bottom: 8),
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(38.0),
                  ),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        offset: const Offset(0, 2),
                        blurRadius: 8.0),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, top: 3, bottom: 3),
                  child: TextField(
                    controller: _txtSearch,
                    onChanged: (String txt) {},
                    style: const TextStyle(
                      fontSize: 15,
                    ),
                    cursorColor: Theme.of(context).primaryColor,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'What do you want to learn?                 ',
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor,
              borderRadius: const BorderRadius.all(
                Radius.circular(38.0),
              ),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.withOpacity(0.4),
                    offset: const Offset(0, 2),
                    blurRadius: 8.0),
              ],
            ),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                borderRadius: const BorderRadius.all(
                  Radius.circular(32.0),
                ),
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  _txtSearch.text.isEmpty
                      ? Fluttertoast.showToast(
                          msg:
                              "You just perform an empty search so we had nothing to show you.",
                          toastLength: Toast.LENGTH_SHORT,
                          timeInSecForIosWeb: 5,
                        )
                      : Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: Genre(
                              title: "Search Result",
                              url: Api.searchUrl + _txtSearch.text,
                            ),
                          ),
                        );
                },
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Icon(Icons.search,
                      size: 25, color: Theme.of(context).backgroundColor),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
