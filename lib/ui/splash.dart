import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../utility/skip_down_time_progress.dart';
import '../utility/slowMo.dart';
import 'landing.dart';
import 'main_activity.dart';
import '../providers/home_provider.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  SlowMoController _controller = SlowMoController("estrellas", speed: 0.55);
  SlowMoController _controller1 = SlowMoController("Tapped", speed: 0.6);
  SlowMoController _controller2 = SlowMoController("Tapped", speed: 0.2);
  bool firstOpen;

  @override
  void initState() {
    super.initState();
  }

  nextPage() async {
    Navigator.pushReplacement(
      context,
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: Main1View(),
      ),
    );
    //Provider.of<HomeProvider>(context, listen: false).getFeeds();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Container(
            //margin: EdgeInsets.only(top: 250, left: 10),
            height: 300,
            width: 325,
            child: FlareActor(
              'assets/flare/crack.flr',
              animation: 'estrellas',
              controller: _controller,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 320, left: 100),
            height: 275,
            width: 275,
            child: FlareActor(
              'assets/flare/TapAnimation.flr',
              animation: 'Tapped',
              controller: _controller1,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 230),
            height: 300,
            width: 350,
            child: FlareActor(
              'assets/flare/TapAnimations.flr',
              animation: 'fireworks',
              controller: _controller2,
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 500),
            child: Image.asset(
              "assets/images/ModelinMilan.PNG",
              width: MediaQuery.of(context).size.width,
              height: 300,
            ),
          ),
          Positioned(
            child: SkipDownTimeProgress(
              color: Colors.red,
              radius: 22.0,
              duration: Duration(seconds: 5),
              size: Size(25.0, 25.0),
              skipText: "Wait",
              onTap: () {
                nextPage();
                // Navigator.pushReplacement(context,
                //     MaterialPageRoute(builder: (context) {
                //   return MainActivity();
                // }));
              },
              onFinishCallBack: (bool value) {
                if (value)
                  nextPage();
                  // Navigator.pushReplacement(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) {
                  //       return MainActivity();
                  //     },
                  //   ),
                  // );
              },
            ),
            top: 50,
            right: 30,
          ),
          Positioned(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(20),
                  alignment: Alignment.centerLeft,
                  width: double.infinity,
                  child: Text(
                    'Be true to yourself and true happiness and success will follow'
                        .toUpperCase(),
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 35.0,
                      fontFamily: "Joyful_Juliana",
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(right: 20),
                  alignment: Alignment.centerRight,
                  width: double.infinity,
                  child: Text(
                    '- milan amiyah',
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Joyful_Juliana",
                    ),
                  ),
                )
              ],
            ),
            left: 0,
            right: 0,
            bottom: MediaQuery.of(context).size.height / 2.2,
          ),
        ],
      ),
    );
  }
}
