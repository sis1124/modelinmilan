import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:modelinmilan/model/pod_model.dart';
import 'package:modelinmilan/model/podcast_model.dart';
import 'package:modelinmilan/providers/podcast_provider.dart';
import 'package:modelinmilan/utility/keys.dart';
import 'package:provider/provider.dart';

import '../utility/constants.dart';
import '../utility/api.dart';

//import '../helper/click_count.dart';
import '../ui/genre.dart';

import '../widgets/book_list_item.dart';
import '../widgets/home_card.dart';
import '../widgets/spotlight.dart';

import '../widgets/pod_card.dart';

class PodCast extends StatefulWidget {


  @override
  _PodCastState createState() => _PodCastState();
}

class _PodCastState extends State<PodCast> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  StreamSubscription _streamSubscription;

  //ClickCount clickCount = ClickCount();
  //final FirebaseAnalytics analytics = FirebaseAnalytics();

  // _HomeState() {
  //   _sendAnalyticsEvent();
  // }
  //
  // Future<void> _sendAnalyticsEvent() async {
  //   await analytics.logAppOpen();
  // }

  // void _play() async {
  //   final playPosition = (position != null &&
  //           duration != null &&
  //           position.inMilliseconds > 0 &&
  //           position.inMilliseconds < duration.inMilliseconds)
  //       ? position
  //       : null;

  //   print("play current**$url");
  //   final result =
  //       //await audioPlayer.play(url, isLocal: isLocal, position: playPosition);
  //       await audioPlayer.play(url);
  //   if (!mounted) {
  //     return;
  //   }
  //   if (result == 1) {
  //     setState(() {
  //       playerState = PlayerState.playing;
  //     });
  //   }

  //   if (Platform.isIOS) {
  //     await audioPlayer.setPlaybackRate(playbackRate: 1.0);
  //   }
  // }

  // void _pause() async {
  //   if (Platform.isIOS) {
  //     final result = await audioPlayer.pause();
  //     if (result == 1) {
  //       setState(() => playerState = PlayerState.paused);
  //     }
  //   } else {
  //     if (duration != null) {
  //       final result = await audioPlayer.pause();
  //       if (result == 1) {
  //         setState(() => playerState = PlayerState.paused);
  //       }
  //     }
  //   }
  // }

  // void _next() async {
  //   curPos = curPos + 1;
  //   if (curPos < curPlayList.length) {
  //     final result = await audioPlayer.stop();
  //     if (result == 1) {
  //       setState(() {
  //         playerState = PlayerState.stopped;
  //       });
  //     }

  //     position = null;
  //     duration = null;
  //     url = curPlayList[curPos].path_mp3;
  //     print(url);
  //     _play();
  //   } else {
  //     curPos = curPos - 1;
  //     scaffoldKey.currentState.showSnackBar(SnackBar(
  //         content: new Text("You are on Last PodCast"),
  //         duration: const Duration(milliseconds: 1500)));
  //   }
  // }

  // void _previous() async {
  //   curPos = curPos - 1;
  //   if (curPos >= 0) {
  //     // _pause();
  //     final result = await audioPlayer.stop();
  //     if (result == 1) {
  //       setState(() {
  //         playerState = PlayerState.stopped;
  //       });
  //     }
  //     position = null;
  //     duration = null;
  //     url = curPlayList[curPos].path_mp3;
  //     print(url);
  //     _play();
  //   } else {
  //     curPos = curPos + 1;
  //     scaffoldKey.currentState.showSnackBar(SnackBar(
  //         content: new Text("You are on First PodCast"),
  //         duration: const Duration(milliseconds: 1500)));
  //   }
  // }

  // void initAudioPlayer() {
  //   audioPlayer = AudioPlayer(mode: mode);

  //   durationSubscription = audioPlayer.onDurationChanged.listen((duration1) {
  //     //print("duration change***$duration1");
  //     if (!mounted) {
  //       return;
  //     }
  //     setState(() => duration = duration1);

  //     // if (Theme.of(context).platform == TargetPlatform.iOS) {
  //     // if (Platform.isIOS) {
  //     //   // set atleast title to see the notification bar on ios.
  //     //   audioPlayer.startHeadlessService();
  //     //
  //     //   audioPlayer.setNotification(
  //     //       title: '$appname',
  //     //       artist: '${curPlayList[curPos].description}',
  //     //       albumTitle: '${curPlayList[curPos].cat_name}',
  //     //       imageUrl: '${curPlayList[curPos].image}',
  //     //       forwardSkipInterval: const Duration(seconds: 30),
  //     //       // default is 30s
  //     //       backwardSkipInterval: const Duration(seconds: 30),
  //     //       // default is 30s
  //     //       duration: duration1,
  //     //       elapsedTime: Duration(seconds: 0));
  //     // }
  //   });

  //   positionSubscription =
  //       audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
  //             position = p;
  //           }));

  //   playerCompleteSubscription = audioPlayer.onPlayerCompletion.listen((event) {
  //     //_next();
  //     if (!mounted) {
  //       return;
  //     }
  //     setState(() {
  //       position = duration;
  //     });
  //   });

  //   playerErrorSubscription = audioPlayer.onPlayerError.listen((msg) {
  //     // print('audioPlayer error : $msg');
  //     if (!mounted) {
  //       return;
  //     }
  //     setState(() {
  //       playerState = PlayerState.stopped;
  //       duration = Duration(seconds: 0);
  //       position = Duration(seconds: 0);
  //     });
  //   });

  //   audioPlayer.onPlayerStateChanged.listen((state) {
  //     // print('audioPlayer state : $state');

  //     if (!mounted) {
  //       return;
  //     }
  //     setState(() {
  //       audioPlayerState = state;
  //     });
  //   });
  //   //  AudioPlayer.logEnabled = true;
  //   audioPlayer.onNotificationPlayerStateChanged.listen((state) {
  //     if (!mounted) return;
  //     setState(() => audioPlayerState = state);
  //   });
  // }

  // @override
  // void initState() {
  //   super.initState();
  //   initAudioPlayer();
  //   AudioPlayer.logEnabled = false;
  // }

  // @override
  // void dispose() {
  //   _streamSubscription.cancel();
  //   playerState = PlayerState.stopped;
  //   audioPlayer.stop();
  //   durationSubscription?.cancel();
  //   positionSubscription?.cancel();
  //   playerCompleteSubscription?.cancel();
  //   playerErrorSubscription?.cancel();
  //   playerStateSubscription?.cancel();

  //   super.dispose();
  // }

  // getBackground() {
  //   return BoxDecoration(
  //     // Box decoration takes a gradient
  //     gradient: LinearGradient(
  //       // Where the linear gradient begins and ends
  //       begin: Alignment.topRight,
  //       end: Alignment.bottomCenter,
  //       // Add one stop for each color. Stops should increase from 0 to 1
  //       //stops: [0.2, 0.4, 0.8],
  //       colors: [
  //         Colors.pink.withOpacity(0.8),
  //         //Color(0xffFD0262).withOpacity(0.8),
  //         Color(0xffFDBF92).withOpacity(0.8),
  //       ],
  //     ),
  //   );
  // }

  // getMediaButton() {
  //   return Container(
  //     height: 100.0,
  //     child: Stack(
  //       alignment: Alignment.center,
  //       children: <Widget>[
  //         Container(
  //             decoration: BoxDecoration(
  //               shape: BoxShape.rectangle,
  //               color: primary.withOpacity(0.1),
  //               borderRadius: BorderRadius.all(Radius.circular(20.0)),
  //             ),
  //             width: MediaQuery.of(context).size.width - 50,
  //             child: Row(
  //               mainAxisAlignment: MainAxisAlignment.spaceAround,
  //               children: <Widget>[
  //                 IconButton(
  //                   icon: Icon(Icons.fast_rewind),
  //                   iconSize: 35,
  //                   onPressed: _previous,
  //                   color: Colors.white,
  //                 ),
  //                 IconButton(
  //                   icon: Icon(Icons.fast_forward),
  //                   iconSize: 35,
  //                   color: Colors.white,
  //                   onPressed: _next,
  //                 ),
  //               ],
  //             )),
  //         Row(
  //           mainAxisSize: MainAxisSize.min,
  //           children: <Widget>[
  //             Container(
  //               decoration: BoxDecoration(
  //                 boxShadow: [
  //                   BoxShadow(color: Colors.white12, offset: Offset(2, 2))
  //                 ],
  //                 shape: BoxShape.circle,
  //                 // Box decoration takes a gradient
  //                 gradient: LinearGradient(
  //                   // Where the linear gradient begins and ends
  //                   begin: Alignment.topRight,
  //                   end: Alignment.bottomLeft,
  //                   // Add one stop for each color. Stops should increase from 0 to 1
  //                   stops: [0.2, 0.5, 0.9],
  //                   colors: [
  //                     Colors.deepOrange.withOpacity(0.5),
  //                     primary.withOpacity(0.7),
  //                     primary,
  //                   ],
  //                 ),
  //               ),
  //               child: Padding(
  //                 padding: const EdgeInsets.all(5.0),
  //                 child: (Platform.isIOS)
  //                     ? (isPlaying == true &&
  //                             playerState == PlayerState.playing)
  //                         ? IconButton(
  //                             icon: Icon(Icons.pause),
  //                             iconSize: 50,
  //                             color: Colors.white,
  //                             onPressed: _pause)
  //                         : IconButton(
  //                             icon: Icon(Icons.play_arrow),
  //                             iconSize: 50,
  //                             color: Colors.white,
  //                             onPressed: _play)
  //                     : (duration == null && playerState != PlayerState.stopped)
  //                         ? Container(
  //                             width: 50,
  //                             height: 50,
  //                             child: CircularProgressIndicator(
  //                                 valueColor: AlwaysStoppedAnimation<Color>(
  //                                     Colors.white)),
  //                           )
  //                         : IconButton(
  //                             icon: Icon(isPlaying == true
  //                                 ? Icons.pause
  //                                 : Icons.play_arrow),
  //                             iconSize: 50,
  //                             color: Colors.white,
  //                             onPressed: isPlaying == true ? _pause : _play,
  //                           ),
  //               ),
  //             ),
  //           ],
  //         )
  //       ],
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Consumer<PodCastProvider>(
      builder: (BuildContext context, PodCastProvider podCastProvider,
          Widget child) {
        return Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              "PodCast",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          body: podCastProvider.loading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : RefreshIndicator(
                  onRefresh: () => podCastProvider.getFeeds(),
                  child: ListView(
                    children: <Widget>[
                      // Container(
                      //   decoration: getBackground(),
                      //   margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      //   height: 100.0,
                      //   width: MediaQuery.of(context).size.width,
                      //   child: Column(
                      //     children: [
                      //       getMediaButton(),
                      //     ],
                      //   ),
                      // ),
                      SizedBox(
                        height: 5,
                      ),
                      GridView.builder(
                        padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                        shrinkWrap: true,
                        physics: new NeverScrollableScrollPhysics(),
                        itemCount: podCastProvider.pod.feed.link.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 1.0,
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          Linkk entrypod = podCastProvider.pod.feed.link[index];
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            child: PodCard(
                              img: entrypod.path_img,
                              title: entrypod.title,
                              index: index,
                              entrypod: entrypod,
                              //play: _play,
                              //pod:PodModel.fromJson(json),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
        );
      },
    );
  }
}
