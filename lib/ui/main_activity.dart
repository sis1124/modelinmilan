import 'dart:io';

import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modelinmilan/providers/download_provider.dart';
import 'package:modelinmilan/providers/home_provider.dart';
import 'package:modelinmilan/providers/podcast_provider.dart';
import 'package:provider/provider.dart';
//import 'package:http/http.dart' as http;
//import 'package:shared_preferences/shared_preferences.dart';

import '../widgets/custom_alert_dialog.dart';
import '../utility/Constants.dart';

import 'download.dart';
import 'home.dart';
import 'settings.dart';
import 'video_home.dart';
import 'pod_cast.dart';
import '../club/club.dart';

class MainActivity extends StatefulWidget {
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<MainActivity> {
  PageController _pageController;
  int _page = 0;
  // String _strSrNo;
  // String _setting = "false";
  //
  // _HomeState() {
  //   loadSharedPrefs();
  // }
  //
  // loadSharedPrefs() async {
  //   try {
  //     SharedPreferences prefs = await SharedPreferences.getInstance();
  //     setState(() {
  //       _strSrNo =
  //           prefs.getString("SrNo") == null ? "0" : prefs.getString("SrNo");
  //     });
  //   } catch (e) {
  //     print(e.toString());
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => exitDialog(context),
      child: Scaffold(
        extendBody: true,
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: _pageController,
          onPageChanged: onPageChanged,
          children: <Widget>[
            Home(),
            VideoHome(),
            PodCast(),
            Club(),
            Profile(),
          ],
        ),
        bottomNavigationBar:
            // BottomNavigationBar(
            //   backgroundColor: Colors.white,
            //   selectedItemColor: Color(0xFFFF00FF),
            //   unselectedItemColor: Colors.grey[500],
            //   elevation: 20,
            //   type: BottomNavigationBarType.fixed,
            //   items: <BottomNavigationBarItem>[
            //     BottomNavigationBarItem(
            //       icon: Icon(
            //         Icons.home,
            //       ),
            //       title: SizedBox(),
            //     ),
            //     BottomNavigationBarItem(
            //       icon: Icon(
            //         Icons.video_library,
            //       ),
            //       title: SizedBox(),
            //     ),
            //     BottomNavigationBarItem(
            //       icon: Icon(
            //         Icons.library_music,
            //       ),
            //       title: SizedBox(),
            //     ),
            //     BottomNavigationBarItem(
            //       icon: Icon(
            //         Icons.file_download,
            //       ),
            //       title: SizedBox(),
            //     ),
            //     BottomNavigationBarItem(
            //       icon: Icon(
            //         Icons.settings,
            //       ),
            //       title: SizedBox(),
            //     ),
            //   ],
            //   onTap: navigationTapped,
            //   currentIndex: _page,
            // ),

            FFNavigationBar(
          theme: FFNavigationBarTheme(
            barBackgroundColor: Colors.white,
            selectedItemBorderColor: Colors.transparent,
            selectedItemBackgroundColor: Colors.green,
            selectedItemIconColor: Colors.white,
            selectedItemLabelColor: Colors.black,
            showSelectedItemShadow: false,
            barHeight: 70,
          ),
          selectedIndex: _page,
          onSelectTab: navigationTapped,
          items: [
            FFNavigationBarItem(
              iconData: Icons.home,
              label: 'Home',
            ),
            FFNavigationBarItem(
              iconData: Icons.video_library,
              label: 'Learning',
              selectedBackgroundColor: Colors.orange,
            ),
            FFNavigationBarItem(
              iconData: Icons.library_music,
              label: 'PodCast',
              selectedBackgroundColor: Colors.purple,
            ),
            FFNavigationBarItem(
              iconData: Icons.supervised_user_circle,
              label: 'Club',
              selectedBackgroundColor: Colors.blue,
            ),
            FFNavigationBarItem(
              iconData: Icons.settings,
              label: 'Setting',
              selectedBackgroundColor: Colors.red,
            ),
          ],
        ),

        // FancyBottomNavigation(
        //   activeIconColor: Colors.white,
        //   circleColor: Color(0xffff00ff),
        //   initialSelection: _page,
        //   inactiveIconColor: Colors.black,
        //
        //   tabs: [
        //     TabData(iconData: Icons.home, title: "Home"),
        //     TabData(iconData: Icons.search, title: "Search"),
        //     TabData(iconData: Icons.shopping_cart, title: "Basket"),
        //     TabData(iconData: Icons.shopping_cart, title: "Basket"),
        //   ],
        //   onTabChangedListener: navigationTapped,
        // ),

        // BubbledNavigationBar(
        //   defaultBubbleColor: Colors.blue,
        //   onTap: (index) {
        //     // handle tap
        //   },
        //   items: <BubbledNavigationBarItem>[
        //     BubbledNavigationBarItem(
        //       icon:       Icon(CupertinoIcons.home, size: 30, color: Colors.red),
        //       activeIcon: Icon(CupertinoIcons.home, size: 30, color: Colors.white),
        //       title: Text('Home', style: TextStyle(color: Colors.white, fontSize: 12),),
        //     ),
        //     BubbledNavigationBarItem(
        //       icon:       Icon(CupertinoIcons.phone, size: 30, color: Colors.purple),
        //       activeIcon: Icon(CupertinoIcons.phone, size: 30, color: Colors.white),
        //       title: Text('Phone', style: TextStyle(color: Colors.white, fontSize: 12),),
        //     ),
        //     BubbledNavigationBarItem(
        //       icon:       Icon(CupertinoIcons.info, size: 30, color: Colors.teal),
        //       activeIcon: Icon(CupertinoIcons.info, size: 30, color: Colors.white),
        //       title: Text('Info', style: TextStyle(color: Colors.white, fontSize: 12),),
        //     ),
        //     BubbledNavigationBarItem(
        //       icon:       Icon(CupertinoIcons.profile_circled, size: 30, color: Colors.cyan),
        //       activeIcon: Icon(CupertinoIcons.profile_circled, size: 30, color: Colors.white),
        //       title: Text('Profile', style: TextStyle(color: Colors.white, fontSize: 12),),
        //     ),
        //   ],
        // ),
      ),
    );
  }

  exitDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => CustomAlertDialog(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 15),
              Text(
                Constants.appName,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
              SizedBox(height: 25),
              Text(
                "Do you really want to quit?",
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                ),
              ),
              SizedBox(height: 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: 40,
                    width: 130,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Text(
                        "Yes",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                      ),
                      onPressed: () => exit(0),
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 130,
                    child: OutlineButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      borderSide:
                          BorderSide(color: Theme.of(context).accentColor),
                      child: Text(
                        "No",
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 16,
                        ),
                      ),
                      onPressed: () => Navigator.pop(context),
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  void navigationTapped(int page) {
    if (page == 2.0) {
      Provider.of<PodCastProvider>(context, listen: false).getFeeds();
      _pageController.jumpToPage(page);
    } else {
      _pageController.jumpToPage(page);
    }
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 0);
    Provider.of<HomeProvider>(context, listen: false).getFeeds();
    Provider.of<DownloadProvider>(context, listen: false).getFeeds();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }
}
