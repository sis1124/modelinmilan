import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../podo/category.dart';
import '../providers/home_provider.dart';
import '../widgets/book_list_item.dart';
class News extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeProvider>(
      builder: (BuildContext context, HomeProvider homeProvider, Widget child) {
        return Scaffold(
          appBar: AppBar(
            title: Text("News"),
          ),
          body: homeProvider.loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : RefreshIndicator(
            onRefresh: () => homeProvider.getFeeds(),
            child: ListView(
              children: [
                SizedBox(
                  height: 10,
                ),
                ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: homeProvider.recent.feed.entrycategory.length,
                  itemBuilder: (BuildContext context, int index) {
                    Entry entrynews = homeProvider.recent.feed.entrycategory[index];
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      child: BookListItem(
                        img: entrynews.coverImage,
                        title: entrynews.title,
                        author: entrynews.category[0],
                        desc: entrynews.summary
                            .replaceAll(RegExp(r"<[^>]*>"), ''),
                        entrynews: entrynews,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        );
      }
    );
  }
}
