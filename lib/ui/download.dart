import 'package:flutter/material.dart';
import 'package:modelinmilan/model/download_model.dart';
import 'package:provider/provider.dart';

import '../providers/download_provider.dart';
import '../widgets/worksheet.dart';

class Downloads extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<DownloadProvider>(
      builder: (BuildContext context, DownloadProvider downloadProvider,
          Widget child) {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              "Downloads",
            ),
          ),
          body: downloadProvider.dw.feed.linkdw.isEmpty
              ? Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset(
                        "assets/images/empty.png",
                        height: 300,
                        width: 300,
                      ),
                      Text(
                        "Nothing is here",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                )
              : GridView.builder(
                  padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                  shrinkWrap: true,
                  itemCount: downloadProvider.dw.feed.linkdw.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 1.0,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    dwLink entry = downloadProvider.dw.feed.linkdw[index];
                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: WorkSheet(
                        img: entry.dw_img,
                        title: entry.title,
                        entry: entry,
                      ),
                    );
                  },
                ),
        );
      },
    );
  }
}
