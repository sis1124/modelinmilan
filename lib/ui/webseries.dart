// import 'package:flutter/material.dart';
// import 'package:modelinmilan/model/webseries_model.dart';
// import 'package:modelinmilan/utility/api.dart';
// import 'package:video_player/video_player.dart';
// import 'package:chewie/chewie.dart';
// import 'package:modelinmilan/widgets/chewie_list_item.dart';

// class Webseries extends StatefulWidget {
//   @override
//   _WebseriesState createState() => _WebseriesState();
// }

// class _WebseriesState extends State<Webseries> {
//   VideoPlayerController _controller;
//   Future<void> _initializeVideoPlayerFuture;


//   void _playVideo() {
//     setState(() {
//       if(_controller.value.isPlaying) {
//         _controller.pause();
//       }
//       else{
//         _controller.play();
//       }
//     });
//   }
  


//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text("Web Series"),),
//       body: Container(
//         height: 250,
//         child: FutureBuilder(
//           future: Api.getWebseries(Api.webseries),
//           builder: (context, snapshot) {
//             if(snapshot.hasData) {
//               return ListView.builder(
//                 itemCount: snapshot.data.length,
//                 shrinkWrap: true,
//                 itemBuilder: (BuildContext context, index) {
//                   WebseriesModel webseries = snapshot.data[index];
//                   return ChewieListItem(
//                     videoPlayerController: VideoPlayerController.network(Api.baseURL + webseries.url),
//                     looping: true,
//                   );
//                 },
//               );
//             }
//             return Center(child: CircularProgressIndicator(),);
//           },
//         ),
//       ),
//     );
//   }
// }