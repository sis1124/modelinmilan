import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../model/comments_data.dart';
import '../providers/comments_provider.dart';
import '../widgets/comment_list.dart';
import '../utility/api.dart';

class Comments extends StatefulWidget {
  final String id;

  Comments({
    Key key,
    @required this.id,
  }) : super(key: key);

  @override
  _CommentsState createState() => _CommentsState();
}

class _CommentsState extends State<Comments> {
  var formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  var textFieldCtrl = TextEditingController();
  String comment;
  String _strSrNo;
  SharedPreferences prefs;

  @override
  void initState() {
    // TODO: implement initState
    loadSharedPrefs();
    super.initState();
  }

  loadSharedPrefs() async {
    try {
      prefs = await SharedPreferences.getInstance();
      setState(() {
        _strSrNo = prefs.getString("SrNo") == null
            ? "Please LogIn"
            : prefs.getString("SrNo");
      });
    } catch (e) {
      print(e.toString());
    }
  }

  void handleSubmit() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      http.post(Api.baseURL + "add-comment.php", body: {
        "user_id": "$_strSrNo",
        "news_id": "${widget.id}",
        "comments": "$comment",
      }).then((res) {
        print(res.body);
        scaffoldKey.currentState.showSnackBar(SnackBar(
            content: new Text(res.body.toString()),
            duration: const Duration(milliseconds: 1500)));
        Provider.of<CommentsProvider>(context, listen: false).getFeeds(widget.id);
      }).catchError((err) {
        print(err);
      });
    }
    print("user_id $_strSrNo");
    print("news_id ${widget.id}");
    print("comments $comment");
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CommentsProvider>(
      builder: (BuildContext context, CommentsProvider detailsProvider,
          Widget child) {
        return Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              "User Comments!",
            ),
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                child: ListView(children: <Widget>[
                  detailsProvider.loading
                      ? Container(
                          height: 100,
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        )
                      : detailsProvider.img.feed.link == null
                          ? Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Image.asset(
                                    "assets/images/empty.png",
                                    height: 300,
                                    width: 300,
                                  ),
                                  Text(
                                    "No comments yet\nBe the first to comment",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: detailsProvider.img.feed.link == null ? 0 : detailsProvider.img.feed.link.length,
                              itemBuilder: (BuildContext context, int index) {
                                Link entry =
                                    detailsProvider.img.feed.link[index];
                                return Padding(
                                  padding: EdgeInsets.symmetric(vertical: 5),
                                  child: CommentListItem(
                                    img: "${entry.path}",
                                    title: "${entry.user}",
                                    author: "${entry.user}",
                                    desc: entry.title
                                        .replaceAll(RegExp(r"<[^>]*>"), ''),
                                  ),
                                );
                              },
                            ),
                ]),
              ),
              Divider(
                height: 1,
                color: Colors.black26,
              ),
              SafeArea(
                child: Container(
                  height: 65,
                  padding:
                      EdgeInsets.only(top: 8, bottom: 10, right: 20, left: 20),
                  width: double.infinity,
                  color: Colors.white,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(25)),
                    child: Form(
                      key: formKey,
                      child: TextFormField(
                        decoration: InputDecoration(
                            errorStyle: TextStyle(fontSize: 0),
                            contentPadding:
                                EdgeInsets.only(left: 15, top: 10, right: 5),
                            border: InputBorder.none,
                            hintText: 'Write a comment',
                            suffixIcon: IconButton(
                              icon: Icon(
                                Icons.send,
                                color: Colors.grey[700],
                                size: 20,
                              ),
                              onPressed: () {
                                handleSubmit();
                              },
                            )),
                        controller: textFieldCtrl,
                        onSaved: (String value) {
                          setState(() {
                            this.comment = value;
                          });
                        },
                        validator: (value) {
                          if (value.length == 0) return 'null';
                          return null;
                        },
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
