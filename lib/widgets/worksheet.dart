import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/providers/home_provider.dart';
import 'package:modelinmilan/providers/member_provider.dart';
import 'package:modelinmilan/ui/membership.dart';
import 'package:modelinmilan/ui_user/login_with_email.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import '../model/download_model.dart';
import '../utility/api.dart';

// import '../providers/details_provider.dart';
// import '../providers/favorites_provider.dart';
// import '../ui/details.dart';
// import '../helper/click_count.dart';

class WorkSheet extends StatefulWidget {
  final String img;
  final String title;
  final dwLink entry;

  WorkSheet({
    Key key,
    @required this.img,
    @required this.title,
    @required this.entry,
  }) : super(key: key);

  static final uuid = Uuid();
  final String imgTag = uuid.v4();
  final String titleTag = uuid.v4();
  final String authorTag = uuid.v4();

  @override
  _WorkSheetState createState() => _WorkSheetState();
}

class _WorkSheetState extends State<WorkSheet> {
  String path = '';
  MemberProvider _memberProvider = MemberProvider();

  @override
  void initState() {
    super.initState();
    getPermission();
  }

  // void _initPath() async {
  //   path = await ExtStorage.getExternalStoragePublicDirectory(ExtStorage.DIRECTORY_DOWNLOADS);
  //   //String fullPath = "$path/${widget.entry.dw_file}";
  //   print(path); 
  // }

  // Future downloadFile(BuildContext context, String url, String filename) async {
  //   if (await Permission.storage.request().isGranted) {
  //     // Either the permission was already granted before or the user just granted it.
  //     final taskId = await FlutterDownloader.enqueue(
  //       url: url,
  //       savedDir: path,
  //       fileName: filename,
  //       showNotification: true,
  //       openFileFromNotification: true,
  //     );
  //   }
  // }

  void getPermission() async {
    await Permission.storage.request().isGranted;
  }
  Future<void> download(Dio dio, String url, String savePath) async {
    ProgressDialog dialog = new ProgressDialog(context);
    dialog.style(
      message: 'downlaoding...\nplease wait...'
    );
    await dialog.show();

    final response = await dio.get(
      url,
      onReceiveProgress: showDownloadProgress,
      //Received data with List<int>
      options: Options(
        responseType: ResponseType.bytes,
        followRedirects: false,
        validateStatus: (status) {
        return status < 500;
      }),
    );

    //write in download folder
    File file = File(savePath);
    var raf = file.openSync(mode: FileMode.write);
    raf.writeFromSync(response.data);
    await raf.close();
    
    await dialog.hide();
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text("Downlaod Completed"),
        duration: new Duration(milliseconds: 2000),
      )
    );
  }

  showDownloadProgress(received, total) {
    if (total != -1) {
      print((received / total * 100).toStringAsFixed(0) + "%");
    }
  }

  @override
  Widget build(BuildContext context) {
    //ClickCount clickCount = ClickCount();
    return InkWell(
      onTap: () {
        // clickCount.showInterstitial();
        // Provider.of<DetailsProvider>(context, listen: false).setEntry(entry);
        // Provider.of<DetailsProvider>(context, listen: false)
        //     .getFeed(entry.related);
        // Navigator.push(
        //   context,
        //   PageTransition(
        //     type: PageTransitionType.rightToLeft,
        //     child: Details(
        //       entry: entry,
        //       imgTag: imgTag,
        //       titleTag: titleTag,
        //       authorTag: authorTag,
        //     ),
        //   ),
        // ).then((v) {
        //   Provider.of<FavoritesProvider>(context, listen: false).getFeed();
        // });
      },
      child: Container(
        width: 175.0,
        height: 280.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                height: 175.0,
                width: 280.0,
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                        child: Hero(
                          tag: widget.imgTag,
                          child: CachedNetworkImage(
                            imageUrl: widget.img,
                            placeholder: (context, url) =>
                                Center(child: CircularProgressIndicator()),
                            errorWidget: (context, url, error) => Image.asset(
                              "assets/images/place.png",
                              fit: BoxFit.cover,
                              height: 175.0,
                              width: 175.0,
                            ),
                            fit: BoxFit.cover,
                            height: 175.0,
                            width: 175.0,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 10.0,
                      right: 10.0,
                      child: SizedBox(
                        width: 50.0,
                        height: 50.0,
                        child: RawMaterialButton(
                          onPressed: () async {
                            var _isMem = Provider.of<HomeProvider>(context, listen: false).getMember();
                            if(_isMem != true) {
                              SharedPreferences prefs = await SharedPreferences.getInstance();
                              if(prefs.getString("profile") == null) {
                                return ChangeScreen(context, LoginWithEmail());
                              }
                              else{
                                return ChangeScreen(context, Membership());
                              }
                            }
                            else {
                              String path = await ExtStorage.getExternalStoragePublicDirectory(ExtStorage.DIRECTORY_DOWNLOADS);
                              String fullPath = "$path/${widget.entry.dw_file}";
                              print(fullPath);
                              Dio dio = Dio();
                              download(dio, widget.entry.dw_url, fullPath);
                              
                            //downloadFile(context, widget.entry.dw_url, widget.entry.dw_file);
                            }
                          },
                          // child: isPlaying
                          //     ? Icon(
                          //   Icons.pause_circle_outline,
                          //   color: Color(0xfff05042),
                          //   size: 40.0,
                          // )
                          child: Icon(
                            Icons.file_download,
                            color: Color(0xfff05042),
                            size: 40.0,
                          ),
                          shape: new CircleBorder(
                              side: BorderSide(
                                  width: 1.0,
                                  color: Color(0xfff05042).withOpacity(0.8))),
                          elevation: 0.0,
                          fillColor: Colors.white.withOpacity(0.8),
                          padding: const EdgeInsets.all(0.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0, bottom: 5.0),
              child: Text(
                "${widget.title.replaceAll(r"\", "")}",
                style: GoogleFonts.poppins(
                  textStyle:
                      TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
