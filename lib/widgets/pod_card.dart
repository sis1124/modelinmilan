import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/pod_model.dart';
import 'package:modelinmilan/providers/home_provider.dart';
import 'package:modelinmilan/providers/member_provider.dart';
import 'package:modelinmilan/ui/membership.dart';
import 'package:modelinmilan/ui/podcast_player.dart';
import 'package:modelinmilan/ui_user/login_with_email.dart';
import 'package:modelinmilan/utility/keys.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import '../model/podcast_model.dart';
import 'package:modelinmilan/ui/podcast_player.dart';

class PodCard extends StatefulWidget {
  final String img;
  final String title;
  final Linkk entrypod;
  final int index;

  //final List<PodModel> _pod;
  List<PodModel> pod;
  

  PodCard({
    Key key,
    //List<PodModel> pod,
    @required this.img,
    @required this.title,
    @required this.entrypod,
    @required this.index,
  });

  @override
  _PodCardState createState() => _PodCardState();
}

class _PodCardState extends State<PodCard> {
  static final uuid = Uuid();
  final String imgTag = uuid.v4();
  final String titleTag = uuid.v4();
  final String authorTag = uuid.v4();

  MemberProvider _memberProvider = MemberProvider();

  @override
  Widget build(BuildContext context) {
    // AudioPlayer audioPlayer = AudioPlayer();
    //ClickCount clickCount = ClickCount();
    return InkWell(
      onTap: () {
        // clickCount.showInterstitial();
        // Provider.of<DetailsProvider>(context, listen: false).setEntry(entry);
        // Provider.of<DetailsProvider>(context, listen: false)
        //     .getFeed(entry.related);
        // Navigator.push(
        //   context,
        //   PageTransition(
        //     type: PageTransitionType.rightToLeft,
        //     child: Details(
        //       entry: entry,
        //       imgTag: imgTag,
        //       titleTag: titleTag,
        //       authorTag: authorTag,
        //     ),
        //   ),
        // ).then((v) {
        //   Provider.of<FavoritesProvider>(context, listen: false).getFeed();
        // });
      },
      child: Container(
        width: 175.0,
        height: 280.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                height: 175.0,
                width: 280.0,
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                        child: Hero(
                          tag: imgTag,
                          child: CachedNetworkImage(
                            imageUrl: "${widget.img}",
                            placeholder: (context, url) =>
                                Center(child: CircularProgressIndicator()),
                            errorWidget: (context, url, error) => Image.asset(
                              "assets/images/place.png",
                              fit: BoxFit.cover,
                              height: 175.0,
                              width: 175.0,
                            ),
                            fit: BoxFit.cover,
                            height: 175.0,
                            width: 175.0,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 10.0,
                      right: 10.0,
                      child: SizedBox(
                        width: 50.0,
                        height: 50.0,
                        child: RawMaterialButton(
                          onPressed: () async {
                            //url ="http://renukatechnologies.in/demo/client/milan/podcast/heart.mp3";
                            
                            
                            // curPos = widget.index;
                            // //curPlayList = _catRadioList;
                            // url = widget.entry.path_mp3;
                            // position = null;
                            // duration = null;
                            // widget._play();

                            // if (isPlaying == true) {
                            //   final result = await audioPlayer.pause();
                            //   if (result == 1) {
                            //     setState(() => playerState = PlayerState.paused);
                            //     print("Song Pause");
                            //   }
                            // } else {
                            //   final result = await audioPlayer.play("http://renukatechnologies.in/demo/client/milan/podcast/heart.mp3");
                            //   if (result == 1) {
                            //     setState(() => playerState = PlayerState.playing);
                            //     print("Song Playing");
                            //   }
                            // }
                          },
                          // child: isPlaying
                          //     ? Icon(
                          //   Icons.pause_circle_outline,
                          //   color: Color(0xfff05042),
                          //   size: 40.0,
                          // )
                          child: GestureDetector(
                            onTap: () async {
                              var _isMem = Provider.of<HomeProvider>(context, listen: false).getMember();
                              if(_isMem != true) {
                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                if(prefs.getString("profile") == null) {
                                  return ChangeScreen(context, LoginWithEmail());
                                }
                                else{
                                  return ChangeScreen(context, Membership());
                                }
                              }
                              else{
                                ChangeScreen(context, MusicPlayer(entrypod: widget.entrypod, index: widget.index, img: widget.img, title: widget.title,));
                              }
                            },
                            child: Icon(
                              Icons.play_circle_outline,
                              color: Color(0xfff05042),
                              size: 40.0,
                            ),
                          ),
                          shape: new CircleBorder(
                              side: BorderSide(
                                  width: 1.0,
                                  color: Colors.white.withOpacity(0.8))),
                          elevation: 0.0,
                          fillColor: Colors.white.withOpacity(0.8),
                          padding: const EdgeInsets.all(0.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0, bottom: 5.0),
              child: Text(
                "${widget.title.replaceAll(r"\", "")}",
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
