import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/ui_user/login_with_email.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

import 'forgot.dart';
import 'registration.dart';

import '../providers/home_provider.dart';
import '../utility/validator.dart';
import '../utility/api.dart';
import '../utility/sharedPref.dart';
import '../model/user_data.dart';
import '../ui/main_activity.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  State<StatefulWidget> createState() {
    return new _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> _key = new GlobalKey();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool _validate = false;
  LoginRequestData _loginData = LoginRequestData();
  RegisterData _registerData = RegisterData();
  bool _obscureText = true;
  SharedPref sharedPref = SharedPref();
  RegisterPage phoneLogin = RegisterPage();

  FocusNode _focusNode;

  final _text = TextEditingController();
  final _phoneController = TextEditingController();
  bool _validatePhone = false;

  @override
  void dispose() {
    super.dispose();
    _focusNode.dispose();
    _text.dispose();
  }

  @override
  void initState() {
    super.initState();
    _focusNode = new FocusNode();
    _focusNode.addListener(_onOnFocusNodeEvent);
  }

  _onOnFocusNodeEvent() {
    setState(() {
      // Re-renders
    });
  }

//This will change the color of the icon based upon the focus on the field
  Color getPrefixIconColor() {
    return _focusNode.hasFocus ? Colors.black : Colors.grey;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF60DFCD),
              Color(0xFF1E9AFE),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: new Center(
          child: new SingleChildScrollView(
            child: new Container(
              margin: new EdgeInsets.all(20.0),
              child: Center(
                child: new Form(
                  key: _key,
                  autovalidate: _validate,
                  child: _getFormUI(),
                ),
              ),
            ),
          ),
        ),
      )
    );
  }

  Widget _getFormUI() {
    return new Column(
      children: <Widget>[
        Text(
          "Welcome",
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            controller: _phoneController,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Mobile Number',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            onSaved: (String value) {
              _loginData.email = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
              autofocus: false,
              obscureText: _obscureText,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                hintText: 'Password',
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                border: InputBorder.none,
                suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() {
                      _obscureText = !_obscureText;
                    });
                  },
                  child: Icon(
                    _obscureText ? Icons.visibility : Icons.visibility_off,
                    color: this.getPrefixIconColor(),
                    semanticLabel:
                        _obscureText ? 'show password' : 'hide password',
                  ),
                ),
              ),
              validator: FormValidator().validatePassword,
              onSaved: (String value) {
                _loginData.password = value;
              }
            ),
        ),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: _sendToServer,
            padding: EdgeInsets.all(12),
            color: Colors.lightBlueAccent,
            child: Text('Log In', style: TextStyle(color: Colors.white)),
          ),
        ),
        Text(
          'OR',
          style: TextStyle(color: Colors.white),
        ),
        new FlatButton(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Login With ',
                style: TextStyle(color: Colors.white),
              ),
              Text(
                'Login With Email',
                style: TextStyle(color: Colors.white, decoration: TextDecoration.underline),
              ),
            ],
          ),
          onPressed: () {
            ChangeScreen(context, LoginWithEmail());
          },
        ),
        new FlatButton(
          child: Text(
            'Forgot password?',
            style: TextStyle(color: Colors.white, decoration: TextDecoration.underline),
          ),
          onPressed: _resetDialogBox,
        ),
        new FlatButton(
          onPressed: _sendToRegisterPage,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Not a member? ', style: TextStyle(color: Colors.white),),
              Text('Sign up now', style: TextStyle(color: Colors.white, decoration: TextDecoration.underline)),
            ],
          ),
        ),
//        new FlatButton(
//          onPressed: _sendToHomePage,
//          child: Text('Skip for now', style: TextStyle(color: Colors.black54)),
//        ),
      ],
    );
  }

  _sendToRegisterPage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RegisterPage()),
    );
  }

  _sendToHomePage() async {
    Navigator.pushReplacement(
      context,
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: MainActivity(),
      ),
    );
    Provider.of<HomeProvider>(context, listen: false).getFeeds();
  }

  _sendToReset() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  _sendToServer() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
      final phone = _phoneController.text.trim();
      phoneLogin.phoneLogin(context, phone);

      http.post(Api.baseURL + "check.php", body: {
        "email": _loginData.email,
        "password": _loginData.password,
      }).then((res) {
        print(res.body.toString());
        final jsonResponse = json.decode(res.body);
        if(jsonResponse["status"]=='fail'){
          scaffoldKey.currentState.showSnackBar(SnackBar(
              content: new Text(jsonResponse["msg"]),
              duration: const Duration(milliseconds: 1500)));
        }else{
          _registerData = RegisterData.fromJson(jsonResponse);
          sharedPref.saveString("name", _registerData.name);
          sharedPref.saveString("fatherName", _registerData.fname);
          sharedPref.saveString("dbo", _registerData.dob);
          sharedPref.saveString("phone", _registerData.phone);
          sharedPref.saveString("email", _registerData.email);
          sharedPref.saveString("address", _registerData.address);
          sharedPref.saveString("town", _registerData.town);
          sharedPref.saveString("state", _registerData.state);
          sharedPref.saveString("district", _registerData.district);
          sharedPref.saveString("pin", _registerData.pincode);
          sharedPref.saveString("SrNo", _registerData.srNo);
          sharedPref.saveString("isLogin", "1");
          sharedPref.saveString("profile", jsonResponse["user"]["profile"]);
          sharedPref.setLogin(true);
          _sendToHomePage();
        }
      }).catchError((err) {
        print(err);
      });
      print("Email ${_loginData.email}");
      print("Password ${_loginData.password}");
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  // Creates an alertDialog for the user to enter their email
  Future<String> _resetDialogBox() {
    return showDialog<String>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CustomAlertDialog(title: "Reset Password");
      },
    );
  }
}

class CustomAlertDialog extends StatefulWidget {
  final String title;

  const CustomAlertDialog({Key key, this.title}) : super(key: key);

  @override
  CustomAlertDialogState createState() {
    return new CustomAlertDialogState();
  }
}

class CustomAlertDialogState extends State<CustomAlertDialog> {
  final _resetKey = GlobalKey<FormState>();
  final _resetEmailController = TextEditingController();
  String _resetEmail;
  bool _resetValidate = false;
  SharedPref sharedPref = SharedPref();

  _sendToForgotPage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ForgotPage()),
    );
  }

  sendOTP(String mobile, String otp) async {
    var url = 'https://control.msg91.com/api/sendotp.php?authkey=264945AeCxvjPPygN5c756de7&mobile=${mobile}&message=Your%20M%20Chandrappa%20App%20Reset%20Password%20OTP%20is%20${otp}&otp=${otp}';
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      var itemCount = jsonResponse['type'];
      print(itemCount);
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  _sendResetEmail() {
    _resetEmail = _resetEmailController.text;

    if (_resetKey.currentState.validate()) {
      _resetKey.currentState.save();

      try {
        // You could consider using async/await here
        return true;
      } catch (exception) {
        print(exception);
      }
    } else {
      setState(() {
        _resetValidate = true;
      });
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AlertDialog(
        title: new Text(widget.title),
        content: new SingleChildScrollView(
          child: Form(
            key: _resetKey,
            autovalidate: _resetValidate,
            child: ListBody(
              children: <Widget>[
                new Text(
                  'Enter the Mobile Number associated with your account.',
                  style: TextStyle(fontSize: 14.0),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                Row(
                  children: <Widget>[
                    new Padding(
                      padding: EdgeInsets.only(top: 8.0),
                      child: Icon(
                        Icons.phonelink_lock,
                        size: 20.0,
                      ),
                    ),
                    new Expanded(
                      child: TextFormField(
                        validator: FormValidator().validatePhone,
                        onSaved: (String val) {
                          _resetEmail = val;
                        },
                        controller: _resetEmailController,
                        keyboardType: TextInputType.phone,
                        autofocus: true,
                        decoration: new InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Mobile Number',
                            contentPadding:
                                EdgeInsets.only(left: 25.0, top: 15.0),
                            hintStyle:
                                TextStyle(color: Colors.black, fontSize: 14.0)),
                        style: TextStyle(color: Colors.black),
                      ),
                    )
                  ],
                ),
                new Column(children: <Widget>[
                  Container(
                    decoration: new BoxDecoration(
                        border: new Border(
                            bottom: new BorderSide(
                                width: 0.5, color: Colors.black))),
                  )
                ]),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          new FlatButton(
            child: new Text(
              'CANCEL',
              style: TextStyle(color: Colors.black),
            ),
            onPressed: () {
              Navigator.of(context).pop("");
            },
          ),
          new FlatButton(
            child: new Text(
              'RESET',
              style: TextStyle(color: Colors.black),
            ),
            onPressed: () {
              if (_sendResetEmail()) {
                String otp = "";
                var rnd = new Random();
                for (var i = 0; i < 4; i++) {
                  otp = otp + rnd.nextInt(9).toString();
                }
                Navigator.of(context).pop(_resetEmail);
                sendOTP(_resetEmail,otp);
                sharedPref.saveString("resetMobile", _resetEmail);
                sharedPref.saveString("resetOTP", otp);
                _sendToForgotPage();
              }
            },
          ),
        ],
      ),
    );
  }
}
