import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/ui_user/login.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

import 'forgot.dart';
import 'registration.dart';

import '../providers/home_provider.dart';
import '../utility/validator.dart';
import '../utility/api.dart';
import '../utility/sharedPref.dart';
import '../model/user_data.dart';
import '../ui/main_activity.dart';


class LoginWithEmail extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginWithEmailState createState() => _LoginWithEmailState();
}

class _LoginWithEmailState extends State<LoginWithEmail> {
  GlobalKey<FormState> _key = new GlobalKey();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool _validate = false;
  LoginRequestData _loginData = LoginRequestData();
  RegisterData _registerData = RegisterData();
  bool _obscureText = true;
  SharedPref sharedPref = SharedPref();

  FocusNode _focusNode;

  final _text = TextEditingController();
  bool _validatePhone = false;

  @override
  void dispose() {
    super.dispose();
    _focusNode.dispose();
    _text.dispose();
  }

  @override
  void initState() {
    super.initState();
    _focusNode = new FocusNode();
    _focusNode.addListener(_onOnFocusNodeEvent);
  }

  _onOnFocusNodeEvent() {
    setState(() {
      // Re-renders
    });
  }

//This will change the color of the icon based upon the focus on the field
  Color getPrefixIconColor() {
    return _focusNode.hasFocus ? Colors.black : Colors.grey;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF60DFCD),
              Color(0xFF1E9AFE),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: new Center(
          child: new SingleChildScrollView(
            child: new Container(
              margin: new EdgeInsets.all(20.0),
              child: Center(
                child: new Form(
                  key: _key,
                  autovalidate: _validate,
                  child: _getFormUI(),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getFormUI() {
    return new Column(
      children: <Widget>[
        Text(
          "Welcome",
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Email',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            onSaved: (String value) {
              _loginData.email = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            autofocus: false,
            obscureText: _obscureText,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Password',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 00.0),
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                child: Icon(
                  _obscureText ? Icons.visibility : Icons.visibility_off,
                  color: this.getPrefixIconColor(),
                  semanticLabel:
                      _obscureText ? 'show password' : 'hide password',
                ),
              ),
            ),
            validator: FormValidator().validatePassword,
            onSaved: (String value) {
              _loginData.password = value;
            }
          ),
        ),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: _sendToServer,
            padding: EdgeInsets.all(12),
            color: Colors.lightBlueAccent,
            child: Text('Log In', style: TextStyle(color: Colors.white)),
          ),
        ),
        Text(
          'OR',
          style: TextStyle(color: Colors.white),
        ),
        new FlatButton(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Login With ", style: TextStyle(color: Colors.white),),
              Text('Phone', style: TextStyle(color: Colors.white, decoration: TextDecoration.underline),),
            ],
          ),
          onPressed: () {
            ChangeScreen(context, LoginPage());
          },
        ),
        new FlatButton(
          onPressed: _sendToRegisterPage,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Don't have account? ", style: TextStyle(color: Colors.white)),
              Text("Register now", style: TextStyle(color: Colors.white, decoration: TextDecoration.underline)),
            ],
          ),
        ),
//        new FlatButton(
//          onPressed: _sendToHomePage,
//          child: Text('Skip for now', style: TextStyle(color: Colors.black54)),
//        ),
      ],
    );
  }

  _sendToRegisterPage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RegisterPage()),
    );
  }

  _sendToHomePage() async {
    Navigator.pushReplacement(
      context,
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: MainActivity(),
      ),
    );
    Provider.of<HomeProvider>(context, listen: false).getFeeds();
  }

  _sendToReset() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  _sendToServer() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
      http.post(Api.baseURL + "check.php", body: {
        "email": _loginData.email,
        "password": _loginData.password,
      }).then((res) {
        print(res.body.toString());
        final jsonResponse = json.decode(res.body);
        if(jsonResponse["status"]=='fail'){
          scaffoldKey.currentState.showSnackBar(SnackBar(
              content: new Text(jsonResponse["msg"]),
              duration: const Duration(milliseconds: 1500)));
        }else{
          _registerData = RegisterData.fromJson(jsonResponse);
          sharedPref.saveString("name", _registerData.name);
          sharedPref.saveString("fatherName", _registerData.fname);
          sharedPref.saveString("dbo", _registerData.dob);
          sharedPref.saveString("phone", _registerData.phone);
          sharedPref.saveString("email", _registerData.email);
          sharedPref.saveString("address", _registerData.address);
          sharedPref.saveString("town", _registerData.town);
          sharedPref.saveString("state", _registerData.state);
          sharedPref.saveString("district", _registerData.district);
          sharedPref.saveString("pin", _registerData.pincode);
          sharedPref.saveString("SrNo", _registerData.srNo);
          sharedPref.saveString("isLogin", "1");
          sharedPref.saveString("profile", jsonResponse["user"]["profile"]);
          sharedPref.setLogin(true);
          _sendToHomePage();
        }
      }).catchError((err) {
        print(err);
      });
      print("Email ${_loginData.email}");
      print("Password ${_loginData.password}");
    } else {
// validation error
      setState(() {
        _validate = true;
      });
    }
  }
}