import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utility/api.dart';
import '../utility/validator.dart';

class ProfilePage extends StatefulWidget {
  @override
  MapScreenState createState() => MapScreenState();
}

class MapScreenState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {
  bool _status = true;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final FocusNode myFocusNode = FocusNode();
  static String isLogin;
  String _strSrNo,
      _strName,
      _strFather,
      _strDate,
      _strMobile,
      _strAddress,
      _strTown,
      _strDistrict,
      _strPin;
  static String _profile;
  DateTime selectedDate = DateTime.now();
  TextEditingController _name;
  TextEditingController _fatherName;
  TextEditingController _address;
  TextEditingController _town;
  TextEditingController _district;
  TextEditingController _pin;
  TextEditingController _phone;
  TextEditingController _date = new TextEditingController();
  GlobalKey<FormState> _key = new GlobalKey();
  SharedPreferences prefs;
  bool _validate = false;
  File _image;
  bool saveStart = false;
  bool saveComplete = false;

  @override
  void initState() {
    // TODO: implement initState
    loadSharedPrefs();
    super.initState();
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
      print('Image Path $_image');
    });
  }

  loadSharedPrefs() async {
    try {
      prefs = await SharedPreferences.getInstance();
      setState(() {
        isLogin = prefs.getString("isLogin") == null
            ? "0"
            : prefs.getString("isLogin");
        _strSrNo = prefs.getString("SrNo") == null
            ? "Please LogIn"
            : prefs.getString("SrNo");
        _strName = prefs.getString("name") == null
            ? "Please LogIn"
            : prefs.getString("name");
        _strFather = prefs.getString("fatherName") == null
            ? "Please LogIn"
            : prefs.getString("fatherName");
        _strDate = prefs.getString("dbo") == null
            ? "Please LogIn"
            : prefs.getString("dbo");
        _strAddress = prefs.getString("address") == null
            ? "Please LogIn"
            : prefs.getString("address");
        _strTown = prefs.getString("town") == null
            ? "Please LogIn"
            : prefs.getString("town");
        _strDistrict = prefs.getString("district") == null
            ? "Please LogIn"
            : prefs.getString("district");
        _strPin = prefs.getString("pin") == null
            ? "Please LogIn"
            : prefs.getString("pin");
        _strMobile = prefs.getString("phone") == null
            ? "Please LogIn"
            : prefs.getString("phone");
        _profile = prefs.getString("profile") == null
            ? null
            : prefs.getString("profile");
        _name = TextEditingController(text: _strName);
        _fatherName = TextEditingController(text: _strFather);
        _date = TextEditingController(text: _strDate);
        _phone = TextEditingController(text: _strMobile);
        _address = TextEditingController(text: _strAddress);
        _town = TextEditingController(text: _strTown);
        _district = TextEditingController(text: _strDistrict);
        _pin = TextEditingController(text: _strPin);
      });
    } catch (e) {
      print(e.toString());
    }
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1950, 8),
        lastDate: DateTime(2025));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _date.value = TextEditingValue(text: picked.toString().split(' ')[0]);
        prefs.setString("dbo", picked.toString().split(' ')[0]);
      });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "Profile",
          ),
        ),
        body: new Container(
          child: new ListView(
            children: <Widget>[
              new Form(
                key: _key,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                      child: new Stack(fit: StackFit.loose, children: <Widget>[
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Container(
                              width: 140.0,
                              height: 140.0,
                              child: ClipOval(
                                child: _image != null
                                    ? Image.file(
                                        _image,
                                        fit: BoxFit.fill,
                                      )
                                    : CachedNetworkImage(
                                        imageUrl: _profile == null
                                            ? ""
                                            : Api.baseURL + _profile,
                                        placeholder: (context, url) => Center(
                                            child: CircularProgressIndicator()),
                                        errorWidget: (context, url, error) =>
                                            Image.asset(
                                          "assets/images/user.png",
                                          fit: BoxFit.cover,
                                          height: 140.0,
                                          width: 140.0,
                                        ),
                                        fit: BoxFit.cover,
                                        height: 140.0,
                                        width: 140.0,
                                      ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 90.0, right: 100.0),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                GestureDetector(
                                  child: new CircleAvatar(
                                    backgroundColor: Colors.red,
                                    radius: 25.0,
                                    child: new Icon(
                                      _status
                                          ? Icons.lock_outline
                                          : Icons.camera_alt,
                                      color: Colors.white,
                                    ),
                                  ),
                                  onTap: () {
                                    !_status
                                        ? getImage()
                                        : scaffoldKey.currentState.showSnackBar(
                                            SnackBar(
                                                content: new Text(
                                                    "Enable Edit Mode to change profile picture"),
                                                duration: const Duration(
                                                    milliseconds: 1500)));
                                  },
                                )
                              ],
                            )),
                      ]),
                    ),
                    new Container(
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 25.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          'Personal Information',
                                          style: TextStyle(
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    new Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        _status
                                            ? _getEditIcon()
                                            : new Container(),
                                      ],
                                    )
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          'Name',
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 2.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Flexible(
                                      child: new TextFormField(
                                        decoration: const InputDecoration(
                                          hintText: "Enter Your Name",
                                        ),
                                        controller: _name,
                                        enabled: !_status,
                                        autofocus: !_status,
                                        validator:
                                            FormValidator().validateTextInput,
                                        onSaved: (String value) {
                                          prefs.setString("name", _name.text);
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          "Father's Name",
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 2.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Flexible(
                                      child: new TextFormField(
                                        controller: _fatherName,
                                        decoration: const InputDecoration(
                                            hintText: "Enter Father's Name"),
                                        enabled: !_status,
                                        validator:
                                            FormValidator().validateTextInput,
                                        onSaved: (String value) {
                                          prefs.setString(
                                              "fatherName", _fatherName.text);
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          "Date of Birth",
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 2.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Flexible(
                                      child: new TextFormField(
                                        controller: _date,
                                        keyboardType: null,
                                        decoration: const InputDecoration(
                                            hintText: "yyyy-mm-dd"),
                                        enabled: !_status,
                                      ),
                                    ),
                                    !_status
                                        ? _getCalenderIcon()
                                        : new Container(),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          'Mobile',
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 2.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Flexible(
                                      child: new TextFormField(
                                        controller: _phone,
                                        decoration: const InputDecoration(
                                            hintText: "Enter Mobile Number"),
                                        enabled: !_status,
                                        validator:
                                            FormValidator().validatePhone,
                                        onSaved: (String value) {
                                          prefs.setString("phone", _phone.text);
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          'Address',
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 2.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Flexible(
                                      child: new TextFormField(
                                        controller: _address,
                                        decoration: const InputDecoration(
                                            hintText: "Enter your address"),
                                        enabled: !_status,
                                        validator:
                                            FormValidator().validateRequired,
                                        onSaved: (String value) {
                                          prefs.setString(
                                              "address", _address.text);
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          'Town',
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 2.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Flexible(
                                      child: new TextFormField(
                                        controller: _town,
                                        decoration: const InputDecoration(
                                            hintText: "Enter Your Town"),
                                        enabled: !_status,
                                        validator:
                                            FormValidator().validateRequired,
                                        onSaved: (String value) {
                                          prefs.setString("town", _town.text);
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        child: new Text(
                                          'District',
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      flex: 2,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: new Text(
                                          'Pin Code',
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      flex: 2,
                                    ),
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 2.0),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Flexible(
                                      child: Padding(
                                        padding: EdgeInsets.only(right: 10.0),
                                        child: new TextFormField(
                                          controller: _district,
                                          decoration: const InputDecoration(
                                              hintText: "Enter State"),
                                          enabled: !_status,
                                          validator:
                                              FormValidator().validateRequired,
                                          onSaved: (String value) {
                                            prefs.setString(
                                                "district", _district.text);
                                          },
                                        ),
                                      ),
                                      flex: 2,
                                    ),
                                    Flexible(
                                      child: new TextFormField(
                                        controller: _pin,
                                        decoration: const InputDecoration(
                                            hintText: "Enter Pin Code"),
                                        enabled: !_status,
                                        validator:
                                            FormValidator().validateRequired,
                                        onSaved: (String value) {
                                          prefs.setString("pin", _pin.text);
                                        },
                                      ),
                                      flex: 2,
                                    ),
                                  ],
                                )),
                            !_status ? _getActionButtons() : new Container(),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myFocusNode.dispose();
    super.dispose();
  }

  Widget _getActionButtons() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 35.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child: new RaisedButton(
                child: saveStart == false
                    ? new Text("Save")
                    : saveComplete == false
                        ? SizedBox(
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                              strokeWidth: 3.0,
                              valueColor:
                                  new AlwaysStoppedAnimation<Color>(Colors.red),
                            ),
                            height: 20.0,
                            width: 20.0,
                          )
                        : new Text("Done!"),
                textColor: Colors.white,
                color: Colors.green,
                onPressed: () {
                  if (_key.currentState.validate()) {
                    setState(() {
                      //loadProgress();
                      saveStart = true;
                      _key.currentState.save();
                      _handleSave();
                      FocusScope.of(context).requestFocus(new FocusNode());
                    });
                  } else {
                    // validation error
                    setState(() {
                      _validate = true;
                    });
                  }
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: new RaisedButton(
                child: new Text("Cancel"),
                textColor: Colors.white,
                color: Colors.red,
                onPressed: () {
                  setState(() {
                    _status = true;
                    FocusScope.of(context).requestFocus(new FocusNode());
                  });
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  _handleSave() async {
    if (_image != null) {
      String base64Image = base64Encode(_image.readAsBytesSync());
      String fileName = _image.path.split("/").last;
      http.post(Api.baseURL + "user-update.php", body: {
        "profile": "true",
        "image": base64Image,
        "image_name": fileName,
        "name": prefs.getString("name"),
        "fname": prefs.getString("fatherName"),
        "dob": prefs.getString("dbo"),
        "phone": prefs.getString("phone"),
        "address": prefs.getString("address"),
        "town": prefs.getString("town"),
        "district": prefs.getString("district"),
        "pincode": prefs.getString("pin"),
        "srno": prefs.getString("SrNo"),
      }).then((res) {
        print(res.body);
        String fileName = _image.path.split("/").last;
        print(fileName);
        setState(() {
          saveComplete = true;
          _status = true;
        });
        prefs.setString("profile", 'images/profile/$fileName');
        scaffoldKey.currentState.showSnackBar(SnackBar(
            content: new Text(res.body.toString()),
            duration: const Duration(milliseconds: 1500)));
      }).catchError((err) {
        print(err);
      });
    } else {
      http.post(Api.baseURL + "user-update.php", body: {
        "profile": "false",
        "name": prefs.getString("name"),
        "fname": prefs.getString("fatherName"),
        "dob": prefs.getString("dbo"),
        "phone": prefs.getString("phone"),
        "address": prefs.getString("address"),
        "town": prefs.getString("town"),
        "district": prefs.getString("district"),
        "pincode": prefs.getString("pin"),
        "srno": prefs.getString("SrNo"),
      }).then((res) {
        print(res.body);
        setState(() {
          saveComplete = true;
          _status = true;
        });
        scaffoldKey.currentState.showSnackBar(SnackBar(
            content: new Text(res.body.toString()),
            duration: const Duration(milliseconds: 1500)));
      }).catchError((err) {
        print(err);
      });
    }
  }

  Widget _getEditIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.red,
        radius: 14.0,
        child: new Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _status = false;
          saveComplete = false;
          saveStart = false;
        });
      },
    );
  }

  Widget _getCalenderIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.red,
        radius: 14.0,
        child: new Icon(
          Icons.calendar_today,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _selectDate(context);
        });
      },
    );
  }
}
