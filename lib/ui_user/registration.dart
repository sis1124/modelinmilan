import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/providers/home_provider.dart';
import 'package:modelinmilan/ui/home.dart';
import 'package:modelinmilan/ui/main_activity.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../utility/validator.dart';
import '../utility/api.dart';
import '../model/user_data.dart';
import '../ui_user/login.dart';

class RegisterPage extends StatefulWidget {
  static String tag = 'register-page';

  // firebase for otp
  final _phoneController = TextEditingController();
  final _codeController = TextEditingController();
  bool _authSuccess = true;
  Future<bool> phoneLogin(BuildContext context, String phone) {
    FirebaseAuth _auth = FirebaseAuth.instance;
    _auth.verifyPhoneNumber(
      phoneNumber: phone,
      timeout: Duration(seconds: 60),
      verificationCompleted: (AuthCredential credential) async {
        await _auth.signInWithCredential(credential);
      },
      verificationFailed: (FirebaseAuthException exception) {
        if(exception.code == null) {
          _authSuccess = true;
        }
        else{
          _authSuccess = false;
        }
        print(exception);
      },
      codeSent: (String verificationId, [int forceResendingToken]) {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              title: Text("Give the Code?"),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: _codeController,
                  ),

                ],
              ),
              actions: [
                MaterialButton(
                  onPressed: () async {
                    PhoneAuthCredential credential = PhoneAuthProvider.credential(
                      verificationId: verificationId,
                      smsCode: _codeController.text.trim()
                    );

                    await _auth.signInWithCredential(credential);
                  },
                  child: Text("Confirm"),
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  color: Colors.cyan,
                  minWidth: MediaQuery.of(context).size.width,
                ),
              ],
            );
          }
        );
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        print(verificationId);
      },
    );
  }

  @override
  State<StatefulWidget> createState() {
    return new _RegisterPageState();
  }
}

class _RegisterPageState extends State<RegisterPage> {
  GlobalKey<FormState> _key = new GlobalKey();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool _validate = false;
  RegisterData _registerData = RegisterData();
  bool _obscureText = true;
  bool _obscureText1 = true;
  File _image;
  DateTime selectedDate = DateTime.now();
  TextEditingController _date = new TextEditingController();

  final TextEditingController _pass = TextEditingController();
  final TextEditingController _confirmPass = TextEditingController();

  FocusNode _focusNode;

  @override
  void dispose() {
    super.dispose();
    _focusNode.dispose();
  }

  @override
  void initState() {
    super.initState();
    _focusNode = new FocusNode();
    _focusNode.addListener(_onOnFocusNodeEvent);
  }

  _onOnFocusNodeEvent() {
    setState(() {
      // Re-renders
    });
  }

//This will change the color of the icon based upon the focus on the field
  Color getPrefixIconColor() {
    return _focusNode.hasFocus ? Colors.black : Colors.grey;
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1950, 8),
        lastDate: DateTime(2025));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _date.value = TextEditingValue(text: picked.toString().split(' ')[0]);
      });
  }
  

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF60DFCD),
              Color(0xFF1E9AFE),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: new Center(
          child: new SingleChildScrollView(
            child: new Container(
              margin: new EdgeInsets.all(20.0),
              child: Center(
                child: new Form(
                  key: _key,
                  //autovalidate: _validate,
                  child: _getFormUI(),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getFormUI() {
    Future getImage() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);

      setState(() {
        _image = image;
        print('Image Path $_image');
      });
    }

    return new Column(
      children: <Widget>[
//        new Image.asset(
//          'assets/images/m_chandrappa.png',
//          height: 100,
//          width: 100,
//        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: CircleAvatar(
                radius: 50,
                backgroundColor: Colors.lightBlueAccent,
                child: ClipOval(
                  child: new SizedBox(
                    width: 100.0,
                    height: 100.0,
                    child: (_image != null)
                        ? Image.file(
                            _image,
                            fit: BoxFit.fill,
                          )
                        : Image.asset(
                            'assets/images/user.png',
                            fit: BoxFit.fill,
                          ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 60.0),
              child: IconButton(
                icon: Icon(
                  Icons.camera,
                  size: 30.0,
                  color: Colors.white,
                ),
                onPressed: () {
                  getImage();
                },
              ),
            ),
          ],
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Full Name',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            validator: FormValidator().validateTextInput,
            onSaved: (String value) {
              _registerData.name = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Father Name',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            //validator: FormValidator().validateTextInput,
            onSaved: (String value) {
              _registerData.fname = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            keyboardType: null,
            controller: _date,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Date of Birth - yyyy-mm-dd',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    _selectDate(context);
                  });
                },
                child: Icon(
                  Icons.calendar_today,
                  color: this.getPrefixIconColor(),
                ),
              ),
            ),
            //validator: FormValidator().validateDOB,
            onSaved: (String value) {
              _registerData.dob = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            controller: widget._phoneController,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Mobile Number',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            validator: FormValidator().validatePhone,
            onSaved: (String value) {
              _registerData.phone = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Email',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            validator: FormValidator().validateEmail,
            onSaved: (String value) {
              _registerData.email = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
              autofocus: false,
              controller: _pass,
              obscureText: _obscureText,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                hintText: 'Password',
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                border: InputBorder.none,
                suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() {
                      _obscureText = !_obscureText;
                    });
                  },
                  child: Icon(
                    _obscureText ? Icons.visibility : Icons.visibility_off,
                    color: this.getPrefixIconColor(),
                    semanticLabel:
                        _obscureText ? 'show password' : 'hide password',
                  ),
                ),
              ),
              validator: FormValidator().validatePassword,
              onSaved: (String value) {
                _registerData.password = value;
              }
            ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
              autofocus: false,
              controller: _confirmPass,
              obscureText: _obscureText1,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                hintText: 'Confirm Password',
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                border: InputBorder.none,
                suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() {
                      _obscureText1 = !_obscureText1;
                    });
                  },
                  child: Icon(
                    _obscureText1 ? Icons.visibility : Icons.visibility_off,
                    color: this.getPrefixIconColor(),
                    semanticLabel:
                        _obscureText1 ? 'show password' : 'hide password',
                  ),
                ),
              ),
              validator: (val) {
                if (val.isEmpty) return 'Password Confirmation Required';
                if (val != _pass.text) return 'Password does not Match';
                return null;
              }
            ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Address',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            validator: FormValidator().validateRequired,
            onSaved: (String value) {
              _registerData.address = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Town',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            validator: FormValidator().validateRequired,
            onSaved: (String value) {
              _registerData.town = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'State',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            validator: FormValidator().validateRequired,
            onSaved: (String value) {
              _registerData.state = value;
            },
          ),
        ),
        new SizedBox(height: 20.0),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0, 
                spreadRadius: 2.0,
                offset: Offset(0.0, 2.0,),
              )
            ]
          ),
          child: new TextFormField(
            keyboardType: TextInputType.number,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Pincode',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: InputBorder.none,
            ),
            validator: FormValidator().validateRequired,
            onSaved: (String value) {
              _registerData.pincode = value;
            },
          ),
        ),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: _sendToServer,
            padding: EdgeInsets.all(12),
            color: Colors.lightBlueAccent,
            child: Text('Register', style: TextStyle(color: Colors.white)),
          ),
        ),
        new FlatButton(
          onPressed: _sendToLoginPage,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Already member? ', style: TextStyle(color: Colors.white),),
              Text('Log In now',
                  style: TextStyle(color: Colors.white, decoration: TextDecoration.underline)),
            ],
          ),
        ),
      ],
    );
  }

  _sendToHomePage() async {
    Navigator.pushReplacement(
      context,
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: MainActivity(),
      ),
    );
    Provider.of<HomeProvider>(context, listen: false).getFeeds();
  }

  _sendToLoginPage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  _sendToServer() {
    if (_key.currentState.validate()) {
      final phone = widget._phoneController.text.trim();
      widget.phoneLogin(context, phone);
      // No any error in validation
      _key.currentState.save();
      if (_image != null) {
        if(widget._authSuccess == true) {
          String base64Image = base64Encode(_image.readAsBytesSync());
          String fileName = _image.path.split("/").last;
          http.post(Api.baseURL + "register.php", body: {
            "profile": "true",
            "image": base64Image,
            "image_name": fileName,
            "name": _registerData.name,
            "fname": _registerData.fname,
            "dob": _registerData.dob,
            "phone": _registerData.phone,
            "email": _registerData.email,
            "pass": _registerData.password,
            "address": _registerData.address,
            "town": _registerData.town,
            //"tahsil": _registerData.tahsil,
            //"district": _registerData.district,
            "state": _registerData.state,
            "pincode": _registerData.pincode,
          }).then((res) {
            print(res.body);
            scaffoldKey.currentState.showSnackBar(SnackBar(
                content: new Text(res.body.toString()),
                duration: const Duration(milliseconds: 1500)));
            //_sendToHomePage();
          }).catchError((err) {
            print(err);
          });
        }
      } else {
        if(widget._authSuccess == true) {
          http.post(Api.baseURL + "register.php", body: {
            "profile": "false",
            "name": _registerData.name,
            "fname": _registerData.fname,
            "dob": _registerData.dob,
            "phone": _registerData.phone,
            "email": _registerData.email,
            "pass": _registerData.password,
            "address": _registerData.address,
            "town": _registerData.town,
            //"tahsil": _registerData.tahsil,
            //"district": _registerData.district,
            "state": _registerData.state,
            "pincode": _registerData.pincode,
          }).then((res) {
            print(res.body);
            scaffoldKey.currentState.showSnackBar(SnackBar(
                content: new Text(res.body.toString()),
                duration: const Duration(milliseconds: 1500)));
            //_sendToHomePage();
          }).catchError((err) {
            print(err);
          });
        }
      }
      print("Phone ${_registerData.phone}");
      print("Password ${_registerData.password}");
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }
}
