import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modelinmilan/model/download_model.dart';
import 'package:modelinmilan/utility/api.dart';

class DownloadProvider with ChangeNotifier {
  String message;
  DownloadData dw = DownloadData();
  bool loading = true;

  getFeeds() async {
    setLoading(true);
    Api.getDownload(Api.downloadURL).then((dw) {
      setDownload(dw);
      setLoading(false);
    }).catchError((e) {
      throw (e);
    });
  }

  void setLoading(value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void setMessage(value) {
    message = value;
    Fluttertoast.showToast(
      msg: value,
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIosWeb: 1,
    );
    notifyListeners();
  }

  String getMessage() {
    return message;
  }

  void setDownload(value) {
    dw = value;
    notifyListeners();
  }

  DownloadData getDownload() {
    return dw;
  }
}
