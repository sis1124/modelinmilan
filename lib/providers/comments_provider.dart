import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../utility/api.dart';
import '../model/comments_data.dart';

class CommentsProvider with ChangeNotifier {
  String message;
  CommentsData img = CommentsData();
  bool loading = true;

  getFeeds(String id) async {
    setLoading(true);
    Api.getComments(Api.commentsUrl+"&id=" + id).then((img) {
      setImg(img);
      setLoading(false);
    }).catchError((e) {
      throw (e);
    });
  }

  void setLoading(value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void setMessage(value) {
    message = value;
    Fluttertoast.showToast(
      msg: value,
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIosWeb: 1,
    );
    notifyListeners();
  }

  String getMessage() {
    return message;
  }

  void setImg(value) {
    img = value;
    notifyListeners();
  }

  CommentsData getImg() {
    return img;
  }
}