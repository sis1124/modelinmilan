import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:modelinmilan/model/user_email_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utility/api.dart';

class ClubProvider with ChangeNotifier {

  UserEmail useremail = UserEmail();
  var isClubMem;
  bool loading;

  getFeed() async {
    setClubLoading(true);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    useremail.email = prefs.getString('email');
    print(useremail.email);
    var body = jsonEncode({'email': useremail.email });
    await http.post(
      Api.clubMem,
      body: body,
    ).then((res) {
      print(res.body.toString());
      var tempMem = json.decode(res.body);
      if(tempMem['status'] == "success") {
        setClubMember(res);
      }
      setClubLoading(false);
    }).catchError((e) {
      throw (e);
    });
  }

  setClubMember(value) {
    isClubMem = json.decode(value.body);
    notifyListeners();
  }
  bool getClubMember() {
    if(isClubMem == null) {
      return false;
    }
    else {
      if(isClubMem['status'] == "success") {
        print("true");
        return true;
      }
      else{
        print("false");
        return false;
      }
    }
  }
   setClubLoading(value) {
     loading = value;
     notifyListeners();
   }
}
