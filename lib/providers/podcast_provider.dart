import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modelinmilan/model/podcast_model.dart';
import 'package:modelinmilan/utility/api.dart';

class PodCastProvider with ChangeNotifier {
  String message;
  PodCastData pod = PodCastData();
  bool loading = true;

  getFeeds() async {
    setLoading(true);
    Api.getPodCast(Api.podcastURL).then((pod) {
      setPodCast(pod);
      setLoading(false);
    }).catchError((e) {
      throw (e);
    });
  }

  void setLoading(value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void setMessage(value) {
    message = value;
    Fluttertoast.showToast(
      msg: value,
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIosWeb: 1,
    );
    notifyListeners();
  }

  String getMessage() {
    return message;
  }

  void setPodCast(value) {
    pod = value;
    notifyListeners();
  }

  PodCastData getPodCast() {
    return pod;
  }
}

