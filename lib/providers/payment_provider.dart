import 'package:flutter/material.dart';
import 'package:stripe_payment/stripe_payment.dart';

class PaymentProvider with ChangeNotifier{
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
  PaymentMethod _paymentMethod = PaymentMethod();


  PaymentProvider.initialize() {
    StripePayment.setOptions(
      StripeOptions(publishableKey: "pk_test_51Hb5RYEVMkNoHaA6mVr0T6iuLDi2TNkBa94e6At9DPm37ENzrq8W9l3yI2EZnvLkgJa2ykoP2PIRmSPJusz4tonA004LN5z3Ee")
    );
  }

  void addCard() {
    StripePayment.paymentRequestWithCardForm(CardFormPaymentRequest()).then((paymentMethod) {
      _paymentMethod = paymentMethod;
    }).catchError((err) {
      print("${err.toString()}");
    });
  }
}