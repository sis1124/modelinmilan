import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:modelinmilan/helpers/screen_navigator.dart';
import 'package:modelinmilan/model/member_model.dart';
import 'package:modelinmilan/model/user_email_model.dart';
import 'package:modelinmilan/ui/membership.dart';
import 'package:modelinmilan/ui_user/login_with_email.dart';
import 'package:modelinmilan/utility/api.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MemberProvider extends ChangeNotifier {
  UserEmail useremail = UserEmail();
  

  Future getFeed(String email) async {
    final response = await http.post(
      Api.member,
      body: {
        'email' : email,
      },
      headers: {"Accept" : "Application/json"},
    );
    var convertedDatatoJson = jsonDecode(response.body);
    print(convertedDatatoJson);
    return convertedDatatoJson;
  }

   getUserEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    useremail.email = prefs.getString('email');
    //print(useremail.email);
    if(useremail.email == null) {
      useremail.email = "null";
    }
    return useremail.email;
  }

  Future checkMem() async {
    var res = await getFeed(getUserEmail().toString());
    // if(res['status'] == 1) {
    //   return true;
    // }
    // else{
    //   return false;
    // }
  }
  Future checkMem2(BuildContext context) async {
    return FutureBuilder(
      future: getFeed(getUserEmail().toString()),
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          MemberModel _memberData = snapshot.data;
          print(_memberData);
        }
      },
    );
  }


}