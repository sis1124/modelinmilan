import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modelinmilan/model/member_model.dart';
import 'package:modelinmilan/model/user_email_model.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../model/download_model.dart';
import '../model/podcast_model.dart';
import '../podo/category.dart';
import '../utility/api.dart';

class HomeProvider with ChangeNotifier {
  String message;
  CategoryFeed top = CategoryFeed();
  // CategoryFeed trends = CategoryFeed();
  CategoryFeed recent = CategoryFeed();
  DownloadData dw = DownloadData();
  
  UserEmail useremail = UserEmail();

  PodCastData podpro = PodCastData();

  bool loading = true;
  bool podloading = true;
  var isMem;
  var userid;

  getFeeds() async {
    setLoading(true);
    Api.getNews(Api.popular).then((popular) {
      setTop(popular);
      Api.getNews(Api.breaking).then((newReleases) {
        setRecent(newReleases);
      }).catchError((e) {
        throw (e);
      });
      Api.getDownload(Api.downloadURL).then((dw) {
        setDownload(dw);
      }).catchError((e) {
        throw (e);
      });
      Api.getPodCast(Api.podcastURL).then((podpro) {
        setPodCast(podpro);
        setPodLoading(false);
      }).catchError((e) {
        throw (e);
      });
    }).catchError((e) {
      throw (e);
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    useremail.email = prefs.getString('email');
    print(useremail.email);
    var body = jsonEncode({'email': useremail.email });
    await http.post(
      Api.member,
      body: body,
    ).then((res) {
      print(res.body.toString());
      var tempMem = json.decode(res.body);
      if(tempMem['status'] == "success") {
        setMember(res);
      }
      else if(useremail.email != null) {
        print(tempMem['user_arr']['SrNo']);
        userid = tempMem['user_arr']['SrNo'];
      }
      setLoading(false);
    }).catchError((e) {
      throw (e);
    });
  }

  void setLoading(value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void setMessage(value) {
    message = value;
    Fluttertoast.showToast(
      msg: value,
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIosWeb: 1,
    );
    notifyListeners();
  }

  String getMessage() {
    return message;
  }

  void setTop(value) {
    top = value;
    notifyListeners();
  }

  CategoryFeed getTop() {
    return top;
  }

  void setRecent(value) {
    recent = value;
    notifyListeners();
  }

  CategoryFeed getRecent() {
    return recent;
  }

  // void setTrends(value) {
  //   trends = value;
  //   notifyListeners();
  // }
  //
  // CategoryFeed getTrends() {
  //   return trends;
  // }
  void setDownload(value) {
    dw = value;
    notifyListeners();
  }

  DownloadData getDownload() {
    return dw;
  }

  void setPodCast(value) {
    podpro = value;
    notifyListeners();
  }
  void setPodLoading(value) {
    podloading = value;
    notifyListeners();
  }

  
  setMember(value) {
    isMem = json.decode(value.body);
    userid = isMem['user_arr']['userid'];
    notifyListeners();
  }
  bool getMember() {
    if(isMem == null) {
      return false;
    }
    else {
      if(isMem['status'] == "success") {
        print("true");
        return true;
      }
      else{
        print("false");
        return false;
      }
    }
  }
  dynamic getUserId() {
    return userid;
  }

}